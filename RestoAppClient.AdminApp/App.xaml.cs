﻿using MvvmCross.Core;
//using MvvmCross.Platforms.Wpf.Core;
using MvvmCross.Platforms.Wpf.ItemsPresenter;

namespace RestoAppClient.AdminApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void RegisterSetup()
        {
            this.RegisterSetupType<Setup>();
        }
    }
}

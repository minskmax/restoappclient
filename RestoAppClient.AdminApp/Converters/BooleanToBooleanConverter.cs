﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RestoAppClient.AdminApp.Converters
{
    public sealed class BooleanToBooleanConverter : BooleanConverter<bool>
    {
        public BooleanToBooleanConverter() :
            base(true, false)
        { }
    }
}

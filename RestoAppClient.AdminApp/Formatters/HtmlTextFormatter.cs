﻿using RichTextBlockSample.HtmlConverter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace RestoAppClient.AdminApp.Formatters
{
    public class HtmlTextFormatter : ITextFormatter
    {
        public string GetText(System.Windows.Documents.FlowDocument document)
        {
            var xml = XamlWriter.Save(document);
            var html = HtmlFromXamlConverter.ConvertXamlToHtml(xml);
            return html;
        }

        public void SetText(System.Windows.Documents.FlowDocument document, string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                document = new FlowDocument();
                return;
            }
            var xml = HtmlToXamlConverter.ConvertHtmlToXaml(text, true);
            var flowDocument = XamlReader.Parse(xml) as FlowDocument;
            flowDocument.FontFamily = new FontFamily("Verdana");
            flowDocument.FontSize = 14;
            AddDocument(flowDocument, document);
        }

        public static void AddDocument(FlowDocument from, FlowDocument to)
        {
            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);
        }
    }
}

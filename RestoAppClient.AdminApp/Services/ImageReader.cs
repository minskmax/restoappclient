﻿using RestoAppClient.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.AdminApp.Services
{
    public class ImageReader : IImageReader
    {
        public async Task<byte[]> LoadImage(string path)
        {
            byte[] result;
            using (FileStream stream = File.Open(path, FileMode.Open))
            {
                result = new byte[stream.Length];
                await stream.ReadAsync(result, 0, (int)stream.Length);
            }
            return result;
        }

        public async Task<bool> SaveImage(string path, byte[] data)
        {
            using (FileStream stream = File.Open(path, FileMode.CreateNew))
            {
                await stream.WriteAsync(data, 0, data.Length);
            }
            return true;
        }

       
    }
}

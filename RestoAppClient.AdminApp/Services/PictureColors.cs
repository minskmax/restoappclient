﻿using RestoAppClient.Core.Interfaces.Helpers;
using RestoAppClient.Core.Interfaces.Native;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RestoAppClient.AdminApp.Services
{
    public class PictureColors:IPictureColors
    {
        public MyColor[] GetThumbColors(byte[] source)
        {
            BitmapImage resizedImage = null;
            using (MemoryStream strmImg = new MemoryStream(source))
            {
                BitmapImage myBitmapImage = new BitmapImage();
                myBitmapImage.BeginInit();
                myBitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                myBitmapImage.StreamSource = strmImg;
                myBitmapImage.DecodePixelWidth = 32;
                myBitmapImage.DecodePixelHeight = 32;
                myBitmapImage.EndInit();
                resizedImage = myBitmapImage;
            }

            int strideSize = 3;
            int stride = resizedImage.PixelWidth*3;
            if (resizedImage.Format.BitsPerPixel == 32)
            {
                stride = resizedImage.PixelWidth * 4;
                strideSize = 4;
            }
            
            int size = resizedImage.PixelHeight * stride;
            byte[] pixels = new byte[size];
            resizedImage.CopyPixels(pixels, stride, 0);

            List<MyColor> colors = new List<MyColor>();
            for (int i = 0; i < pixels.Length/strideSize; i++)
            {
                var color = System.Drawing.Color.FromArgb(
                    pixels[(i * strideSize) + 2],
                    pixels[(i * strideSize) + 1],
                    pixels[(i * strideSize)]
                    );

                var myColor = new MyColor
                {
                    R = color.R,
                    G = color.G,
                    B = color.B,
                    H = color.GetHue()/360f,
                    S = color.GetSaturation(),
                    V = color.GetBrightness()
                };

                colors.Add(myColor);
            }

            return FilterColors(colors).ToArray();
        }

        private MyColor[] FilterColors(IEnumerable<MyColor> input)
        {
            var filter = input.Where(x => x.V > 0.28f && x.S > 0.5f).GroupBy(q=>q).OrderByDescending(q=>q.Count()).Take(400).Select(g=>g.Key).OrderByDescending(g=>g.S).Take(1);            
            var filterBack = input.GroupBy(q => q).OrderByDescending(q => q.Count()).Take(1).Select(g => g.Key).Take(1);
            var result = filter.ToArray();
            for (int i = 0; i < filter.Count(); i++)
            {
                result[i].V = 0.35f;
                var resColor = HSL2RGB(result[i].H, result[i].S, result[i].V);
                resColor.H = result[i].H;
                resColor.S = result[i].S;
                resColor.V = result[i].V;
                result[i] = resColor;
            }
            var resultBack = filterBack.ToArray();
            for (int i = 0;i<filterBack.Count();i++)
            {
                resultBack[i].V = 0.15f;
                var resColor = HSL2RGB(resultBack[i].H, resultBack[i].S, resultBack[i].V);
                resColor.H = resultBack[i].H;
                resColor.S = resultBack[i].S;
                resColor.V = resultBack[i].V;
                resultBack[i] = resColor;
            }
            var final = new List<MyColor>();
            final.AddRange(result);
            final.AddRange(resultBack);
            for (int i = 0; i < final.Count; i++)
            {
                var color = final[i];
                color.color = ColorToUInt(final[i].R, final[i].G, final[i].B);                
                final[i] = color;
            }
            return final.ToArray();
        }

        public static MyColor HSL2RGB(double h, double sl, double l)
        {
            double v;
            double r, g, b;

            r = l;   // default to gray
            g = l;
            b = l;
            v = (l <= 0.5) ? (l * (1.0 + sl)) : (l + sl - l * sl);
            if (v > 0)
            {
                double m;
                double sv;
                int sextant;
                double fract, vsf, mid1, mid2;

                m = l + l - v;
                sv = (v - m) / v;
                h *= 6.0;
                sextant = (int)h;
                fract = h - sextant;
                vsf = v * sv * fract;
                mid1 = m + vsf;
                mid2 = v - vsf;
                switch (sextant)
                {
                    case 0:
                        r = v;
                        g = mid1;
                        b = m;
                        break;
                    case 1:
                        r = mid2;
                        g = v;
                        b = m;
                        break;
                    case 2:
                        r = m;
                        g = v;
                        b = mid1;
                        break;
                    case 3:
                        r = m;
                        g = mid2;
                        b = v;
                        break;
                    case 4:
                        r = mid1;
                        g = m;
                        b = v;
                        break;
                    case 5:
                        r = v;
                        g = m;
                        b = mid2;
                        break;
                }
            }
            MyColor rgb = new MyColor
            {
                R = Convert.ToByte(r * 255.0f),
                G = Convert.ToByte(g * 255.0f),
                B = Convert.ToByte(b * 255.0f)
            };
            return rgb;
        }

        private int ColorToUInt(byte r, byte g, byte b)
        {
            return (int)((r << 16) |
                          (g << 8) | (b << 0));
        }
    }
}

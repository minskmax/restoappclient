﻿using MvvmCross;
using MvvmCross.Platforms.Wpf.ItemsPresenter;
using RestoAppClient.AdminApp.Services;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Services.TokenStores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.AdminApp
{
    public class Setup : MvxWpfSetup<Core.AdminApp>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
            Mvx.IoCProvider.RegisterType<ITokenStore, WpfTokenStore>();
            Mvx.IoCProvider.RegisterType<IFileDialogService, FileDialogService>();
            Mvx.IoCProvider.RegisterType<IImageReader, ImageReader>();
            Mvx.IoCProvider.RegisterType<IPictureColors, PictureColors>();
        }
    }

}

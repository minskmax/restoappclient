﻿using MvvmCross;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Services;
using RestoAppClient.Core.Services.Rest;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Core.ViewModels.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core
{
    public class AdminApp : MvxApplication
    {
        public override void Initialize()
        {            
            Mvx.IoCProvider.RegisterSingleton<IRestClient>(() => new RestSharpClient("http://u0569536.plsk.regruhosting.ru/api/", "restoapp", "secret"));
            Mvx.IoCProvider.RegisterType<INewsService, NewsService>();
            Mvx.IoCProvider.RegisterType<IImageService, ImageService>();

            RegisterAppStart<PreloadViewModel>();
            base.Initialize();
        }
    }
}

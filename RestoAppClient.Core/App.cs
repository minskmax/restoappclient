﻿using AutoMapper;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Mapping;
using RestoAppClient.Core.Services;
using RestoAppClient.Core.Services.Collections;
using RestoAppClient.Core.Services.Fakers;
using RestoAppClient.Core.Services.Rest;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Core.ViewModels.Items;

namespace RestoAppClient.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterSingleton<IRestClient>(() => new RestSharpClient("http://u0569536.plsk.regruhosting.ru/api/", "restoapp", "secret"));
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<INewsService, NewsService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IBonusesService, BonusesService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IImageService, ImageService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IUserService, UserService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IFakeMaker<NewsItem>, NewsFaker>();            
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<NewsCollection, NewsCollection>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<TransactionsCollection, TransactionsCollection>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IFirebaseService, FirebaseService>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IAuthorizationService, AuthorizationService>();


            Mapper.Initialize(cfg =>
            {
                cfg.AddProfiles(typeof(ImageMapping));                
            });

            RegisterAppStart<NewsListViewModel>();
        }
    }
}
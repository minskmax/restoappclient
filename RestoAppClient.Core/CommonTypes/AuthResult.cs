﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.CommonTypes
{
    public enum AuthResult
    {
        Success,
        ConnectionError,
        TokenCheckError,
        NoTokenProvided
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Exceptions
{
    public class ConnectionErrorException:Exception
    {
        public ConnectionErrorException()
        {
        }

        public ConnectionErrorException(string message)
            : base(message)
        {
        }

        public ConnectionErrorException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}

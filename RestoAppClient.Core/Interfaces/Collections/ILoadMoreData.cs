﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Collections
{
    public interface ILoadMoreData
    {
        bool Loading { get; }
        Task LoadMoreDataAsync(bool refresh);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Interfaces.Helpers
{
    public struct MyColor
    {
        public byte R;
        public byte G;
        public byte B;
        public float H;
        public float S;
        public float V;
        public int color;
    }
}

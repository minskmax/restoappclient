﻿using RestoAppClient.Core.CommonTypes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces
{
    public interface IAuthorizationService
    {
        Task<AuthResult> Authorize();
        bool IsAuthorized { get; }
        bool IsAdmin { get; }
    }
}

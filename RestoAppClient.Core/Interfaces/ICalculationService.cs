﻿
namespace RestoAppClient.Core.Interfaces
{
    public interface ICalculationService
    {
        double TipAmount(double subTotal, int generosity);
    }
}
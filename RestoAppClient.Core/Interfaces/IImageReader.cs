﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces
{
    public interface IImageReader
    {
        Task<byte[]> LoadImage(string path);
        Task<bool> SaveImage(string path, byte[] data);
    }
}

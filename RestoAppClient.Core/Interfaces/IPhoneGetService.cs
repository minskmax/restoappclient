﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces
{
    public interface IPhoneGetService
    {
        Task<string> GetPhoneNumber();
    }
}

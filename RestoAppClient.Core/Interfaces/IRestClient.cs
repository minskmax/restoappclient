﻿using RestoAppClient.Core.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces
{
    public interface IRestClient
    {
        string BaseUrl { get;}

        void UpdateToken(string token);
        Task<T> ExecuteAsync<T>(RestRequest request) where T : new();
        Task<T> ExecuteAsync<T>(RestRequest request, int timeout) where T : new();
        Task<HttpStatusCode> ExecuteAsync(RestRequest request);
        Task<string> RequestToken(string phone, string code);
        Task<User> RequestCode(string phone);
        Task<bool> СheckAdminToken(string token);
        Task<bool> CheckToken(string token);
        string CurrentToken();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Native
{
    public interface IBitmapConverter
    {
        object NativeBlankImage { get; }
        Task<object> FromByteArray(byte[] data);
    }
}

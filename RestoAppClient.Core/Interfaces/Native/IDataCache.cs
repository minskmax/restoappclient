﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Native
{
    public interface IDataCache
    {
        Task<bool> UpdateData(int key, string value, DateTime modified, DateTime created);        
        Task<string> TryLoadData(int key);
        Task<bool> TryGetSpace(int id,DateTime created);
        Task<bool> IsNeedUpdateAsync(int id, DateTime modified);
        Task<IEnumerable<KeyValuePair<int, string>>> LoadCache();
    }
}

﻿using RestoAppClient.Core.Interfaces.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Interfaces.Native
{
    public interface IPictureColors
    {
        MyColor[] GetThumbColors(byte[] source);
    }
}

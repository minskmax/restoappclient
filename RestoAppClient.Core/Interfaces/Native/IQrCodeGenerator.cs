﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Interfaces.Native
{
    public interface IQrCodeGenerator
    {
        object GenerateQrCode(string url,int width=300);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Interfaces.Native
{
    public interface IToaster
    {
        void ShowMessage(string header, string body, Action onClick);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Interfaces.Native
{
    public interface ITokenAccess
    {
        string GetFirebaseToken();
    }
}

﻿using RestoAppClient.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Rest
{
    public interface IBonusesService
    {
        Task<IEnumerable<BonusTransaction>> GetTransactions(string phone = null, int count = 100);
        Task<BonusTransaction> AddTransactionAsync(BonusTransaction item);
        Task<BonusTransaction> UpdateTransactionAsync(BonusTransaction item);
        Task<bool> DeleteTransactionAsync(int id);
        Task<int> GetInactiveBonuses(string userId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Rest
{
    public interface IFirebaseService
    {
        Task<bool> UpdateToken(string token);
    }
}

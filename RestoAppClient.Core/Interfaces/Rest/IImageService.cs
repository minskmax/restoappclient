﻿using RestoAppClient.Core.Models;
using RestoAppClient.Core.Models.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Rest
{
    public interface IImageService
    {
        Task<IEnumerable<ImageInfo>> GetImageIdsForNews(int newsId);
        Task<ImageInfo> LoadThumbnail(int imageId);
        Task<ImageInfo> LoadImage(int imageId);
        Task<int> AttachImageToNews(int newsId, byte[] Image);
        Task<bool> DeleteImage(int imageId);
    }
}

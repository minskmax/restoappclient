﻿using RestoAppClient.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Rest
{
    public interface INewsService
    {
        Task<IEnumerable<News>> GetNewsAsync(int start, int limit);
        Task<News> AddNewsAsync(News item);
        Task<News> UpdateNewsAsync(News item);
        Task<bool> DeleteNewsAsync(int id);
        Task<bool> PingAsync();
        Task<int> Count();
        Task<bool> TrySetColors(int id, int accentColor, int backColor);
    }
}

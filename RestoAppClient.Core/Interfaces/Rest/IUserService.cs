﻿using RestoAppClient.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Interfaces.Rest
{
    public interface IUserService
    {
        Task<bool> RequestCode(string phone);
        Task<string> RequestToken(string phone, string code);
        Task<bool> CheckToken(string token);

        Task<User> CurrentUser();
        Task<User> UpdateCurrentUser();
        Task<User> UserById(string id);
    }
}

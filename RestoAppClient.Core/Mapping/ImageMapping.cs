﻿using AutoMapper;
using MvvmCross;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using RestoAppClient.Core.Models.Dto;
using RestoAppClient.Core.ViewModels.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Mapping
{
    public class ImageMapping:Profile
    {
        public ImageMapping()
        {
            CreateMap<ImageInfo, ImageItem>()
                .ConstructUsing(x => new ImageItem(Mvx.IoCProvider.Resolve<IImageService>(),x.ImageId))
                .ForMember(o => o.Id, opt => opt.MapFrom(src => src.ImageId))
                .ForMember(o => o.NewsId, opt => opt.MapFrom(src => src.NewsId))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}

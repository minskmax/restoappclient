﻿using AutoMapper;
using RestoAppClient.Core.Models;
using RestoAppClient.Core.ViewModels.Items;
using System;

namespace RestoAppClient.Core.Mapping
{
    public class TransactionsMapping : Profile
    {
        public TransactionsMapping()
        {
            CreateMap<BonusTransaction, TransactionItem>()
                    .ForMember(o => o.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(o => o.Phone, opt => opt.MapFrom(src => src.UserPhone))
                    .ForMember(o => o.Count, opt => opt.MapFrom(src => src.Count))
                    .ForMember(o => o.Cause, opt => opt.MapFrom(src => src.Cause))                    
                    .ForMember(o => o.CreatorId, opt => opt.MapFrom(src => src.CreatorId))
                    .ForMember(o => o.Date, opt => opt.MapFrom(src => DateTime.SpecifyKind(src.Date, DateTimeKind.Utc)))
                    .ForMember(o => o.OwnerId, opt => opt.MapFrom(src=>src.UserId))
                    .ReverseMap()
                    .ForMember(o => o.Date, opt => opt.MapFrom(src => src.Date))
                    .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}


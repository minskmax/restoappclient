﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models
{
    public class BonusTransaction
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserPhone { get; set; }
        public string Cause { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }
        public string CreatorId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models.Dto
{
    public class ColorSet
    {
        public int Id { get; set; }
        public int AccentColor { get; set; }
        public int BackColor { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models.Dto
{
    public class FirebaseTokenUpdate
    {
        public string Token { get; set; }
    }
}

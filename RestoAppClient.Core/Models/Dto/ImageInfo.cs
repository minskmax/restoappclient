﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models.Dto
{
    public class ImageInfo
    {
        public int ImageId { get; set; }
        public int NewsId { get; set; }
        public string Data { get; set; }
    }
}

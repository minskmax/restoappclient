﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models.Dto
{
    public class TransactionArgs
    {
        public string Phone { get; set; }        

        public TransactionArgs(string phone)
        {
            Phone = phone;         
        }
    }
}

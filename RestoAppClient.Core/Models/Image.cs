﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models
{
    public class Image
    {
        public int? Id { get; set; }
        public int NewsId { get; set; }
        public byte[] Data { get; set; }
        public byte[] Thumbnail { get; set; }
    }
}

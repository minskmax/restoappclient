﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models
{
    public class News
    {
        public int? Id { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public bool Push { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public int? AccentColor { get; set; }
        public int? BackColor { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Phone { get; set; }
        public int Bonuses { get; set; }
        public int TotalBonuses { get; set; }
        public string LevelName { get; set; }
        public string LevelColor { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Models
{
    public class UserLogin
    {
        public string Phone { get; set; }
        public string Code { get; set; }
    }
}

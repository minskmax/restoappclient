﻿using RestoAppClient.Core.CommonTypes;
using RestoAppClient.Core.Interfaces;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        private IRestClient _restClient;
        private ITokenStore _tokenStore;

        private bool _authComplited;

        public bool IsAdmin { get; private set; } = false;

        public AuthorizationService(IRestClient client, ITokenStore tokenStore)
        {
            _restClient = client;
            _tokenStore = tokenStore;
        }

        public bool IsAuthorized
        {
            get
            {
                return _authComplited;
            }
        }

        public async Task<AuthResult> Authorize()
        {
            if (_authComplited) return AuthResult.Success;
            if (string.IsNullOrEmpty(_restClient.CurrentToken()))//Try load saved token from token store
            {
                var storedToken = await _tokenStore.LoadTokenAsync();
                if (string.IsNullOrEmpty(storedToken)) return AuthResult.NoTokenProvided;
                _restClient.UpdateToken(storedToken);//Provide loaded token to Rest Service
            }

            var tokenValid = await _restClient.CheckToken(_restClient.CurrentToken()); //Check Token valid
            IsAdmin = await _restClient.СheckAdminToken(_restClient.CurrentToken()); //Check if user is admin

            if (tokenValid) _authComplited = true;
            return tokenValid ? AuthResult.Success : AuthResult.TokenCheckError;
        }
    }
}

﻿using MvvmCross.ViewModels;
using RestoAppClient.Core.Helpers;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Collections;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.ViewModels.Items;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services.Collections
{
    public class NewsCollection : MvxObservableCollection<NewsItem>, ILoadMoreData
    {
        private const int _maxCount = 100;
        private bool _loading;
        public bool Loading { get { return _loading; } private set { _loading = value; OnLoadingChanged(); } }

        private static readonly AsyncLock _mutex = new AsyncLock();

        public event EventHandler LoadingChanged;
        private void OnLoadingChanged()
        {
            LoadingChanged?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler NeedUpdate;
        private void OnNeedUpdate()
        {
            NeedUpdate?.Invoke(this, EventArgs.Empty);
        }


        private INewsService _newsService;
        private IImageService _imageService;
        private IFakeMaker<NewsItem> _faker;
        private IDataCache _cache;

        private int _pageSize;
        private int _lastloadIndex;
        private int? _totalCount;

        public NewsCollection(INewsService service, IFakeMaker<NewsItem> faker, IImageService imageService, IDataCache cache, int pageSize = 10) : base()
        {
            _totalCount = null;
            _newsService = service;
            _pageSize = pageSize;
            _imageService = imageService;
            _faker = faker;
            _cache = cache;

            //Get some fake news for start         
        }

        public async Task LoadCache()
        {
            var cache = await _faker.GetCollection(10).ConfigureAwait(false);
            if (cache != null && cache.Count() > 0)
            {
                AddRange(cache);
                await InvokeOnMainThread(() => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset))).ConfigureAwait(false);
            }
        }

        public async Task RefreshData()
        {
            if (!Loading)
            {
                _totalCount = null;
                _lastloadIndex = 0;
                await LoadMoreDataAsync(true).ConfigureAwait(false);
            }
        }

        public async Task LoadMoreDataAsync(bool refresh)
        {
            using (await _mutex.LockAsync().ConfigureAwait(false))
            {
                Loading = true;
                var nonFakeCount = this.Count(x => x.Id != null);
                if (_totalCount == null)
                {
                    _totalCount = await _newsService.Count().ConfigureAwait(false);
                }

                if ((_totalCount > nonFakeCount && nonFakeCount < _maxCount) || refresh)
                {
                    var loadCount = refresh ? this.Count : _pageSize;
                    var newDtos = await _newsService.GetNewsAsync(_lastloadIndex, loadCount).ConfigureAwait(false);
                    List<NewsItem> newItems = new List<NewsItem>();

                    if (newDtos != null)
                    {
                        //Remove fake news
                        //await InvokeOnMainThread(() =>
                        //{
                        RemoveItems(this.Where(x => x.Id == null).ToList());
                        //}).ConfigureAwait(false);

                        foreach (var item in newDtos)
                        {
                            NewsItem adding = new NewsItem(_imageService, (int)item.Id)
                            {
                                Id = item.Id,
                                Header = item.Header,
                                Body = item.Body,
                                Created = item.Created,
                                Modified = item.Modified,
                                AccentColor = item.AccentColor,
                                BackColor = item.BackColor
                            };

                            if (adding.AccentColor == null) adding.AccentColor = ColorToInt(135, 0, 0);
                            if (adding.BackColor == null) adding.BackColor = ColorToInt(33, 33, 33);

                            newItems.Add(adding);

                            _lastloadIndex++;
                        }

                        if (newItems.Count > 0)
                        {
                            DateTime? lastCreated = null;

                            if (Items != null && Items.Count > 0)
                            {
                                lastCreated = Items?[0]?.Created;
                            }

                            var top = 0;

                            foreach (var item in newItems)
                            {
                                var exists = Items.Where(x => x.Id == item.Id).SingleOrDefault();
                                var existsIndex = Items.IndexOf(exists);

                                if (exists == null)
                                {
                                    if (lastCreated != null && DateTime.Compare((DateTime)lastCreated, item.Created) < 0)
                                    {
                                        Insert(top, item);
                                        top++;
                                        //lastCreated = item.Created;                                    
                                        OnNeedUpdate();
                                        await InvokeOnMainThread(() => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset))).ConfigureAwait(false);
                                    }
                                    else
                                    {
                                        Add(item);
                                    }
                                }
                                else if (DateTime.Compare(exists.Modified, item.Modified) < 0) //Replace item if modified
                                {
                                    Items[existsIndex] = item;
                                    await Items[existsIndex].RaiseAllPropertiesChanged();
                                }
                                else
                                {
                                    item.ImagesLoaded = true;
                                }
                            }
                        }
                    }
                    foreach (var item in newItems)
                    {
                        if (!item.ImagesLoaded)
                        {
                            await item.LoadImages().ConfigureAwait(false);
                            //Caching
                            var needUpdate = await _cache.IsNeedUpdateAsync((int)item.Id, item.Modified).ConfigureAwait(false);
                            var spaceAvailable = await _cache.TryGetSpace((int)item.Id, item.Created).ConfigureAwait(false);
                            if (needUpdate && spaceAvailable)
                            {
                                var cacheResult = await _cache.UpdateData((int)item.Id, item.SerializeToJson(), item.Modified, item.Created).ConfigureAwait(false);
                            }
                        }
                    }
                }
                Loading = false;
            }
        }
            private int ColorToInt(byte r, byte g, byte b)
            {
                return (int)((r << 16) |
                              (g << 8) | (b << 0));
            }
        }
    }

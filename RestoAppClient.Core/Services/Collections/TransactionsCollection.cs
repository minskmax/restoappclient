﻿using AutoMapper;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.ViewModels.Items;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services.Collections
{
    public class TransactionsCollection : MvxObservableCollection<TransactionItem>
    {
        private IBonusesService _service;

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { _phone = string.IsNullOrEmpty(value) ? null : value; }
        }

        public TransactionsCollection(IBonusesService service)
        {
            _service = service;
        }

        public async Task Reload()
        {
            Clear();
            var transactions = await _service.GetTransactions(_phone).ConfigureAwait(false);

            if (transactions == null) return;

            AddRange(Mapper.Map<IEnumerable<TransactionItem>>(transactions));
        }
    }
}

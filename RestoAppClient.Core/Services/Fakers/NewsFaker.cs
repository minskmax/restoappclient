﻿using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.ViewModels.Items;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services.Fakers
{
    public class NewsFaker : IFakeMaker<NewsItem>
    {
        private IImageService _imageService;
        //private IBitmapConverter _converter;
        private IDataCache _cache;

        public NewsFaker(IDataCache cache,IImageService imageService)
        {
            _imageService = imageService;
            //_converter = converter;
            _cache = cache;
        }

        public async Task<IEnumerable<NewsItem>> GetCollection(int count)
        {
            var items = await _cache.LoadCache().ConfigureAwait(false);
            var result = new List<NewsItem>();
            foreach (var item in items)
            {
                var news = NewsItem.FromJson(item.Value);
                result.Add(news);
                //foreach (var image in news.Images)
                //{
                //    await image.RefreshImage();
                //}
            }
            return result;
        }        
    }
}

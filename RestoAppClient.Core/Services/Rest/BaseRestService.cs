﻿using RestoAppClient.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestoAppClient.Core.Services.Rest
{
    public abstract class BaseRestService
    {
        protected IRestClient _client;

        public BaseRestService(IRestClient client)
        {
            _client = client;
        }
    }
}

﻿using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using RestSharp;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services.Rest
{
    public class BonusesService : BaseRestService, IBonusesService
    {
        public BonusesService(Interfaces.IRestClient client) : base(client)
        {
        }

        public async Task<BonusTransaction> AddTransactionAsync(BonusTransaction item)
        {
            var request = new RestRequest
            {
                Resource = "bonuses",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(item);

            var response = await _client.ExecuteAsync<BonusTransaction>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<bool> DeleteTransactionAsync(int id)
        {
            var request = new RestRequest
            {
                Resource = "bonuses/delete",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(id);

            var response = await _client.ExecuteAsync<HttpStatusCode>(request).ConfigureAwait(false);
            if (response == HttpStatusCode.OK) return true;
            return false;
        }

        public async Task<int> GetInactiveBonuses(string userId)
        {
            if (string.IsNullOrEmpty(userId)) return -1;                       

            var request = new RestRequest
            {
                Resource = $"bonuses/inactive?userId={userId}",
                RequestFormat = DataFormat.Json
            };

            var response = await _client.ExecuteAsync<int>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<IEnumerable<BonusTransaction>> GetTransactions(string phone = null, int count = 100)
        {
            if (phone == null) phone = string.Empty;
            else
                phone = WebUtility.UrlEncode(phone);

            var request = new RestRequest
            {
                Resource = $"bonuses/list?phone={phone}&count={count}",
                RequestFormat = DataFormat.Json
            };

            var response = await _client.ExecuteAsync<List<BonusTransaction>>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<BonusTransaction> UpdateTransactionAsync(BonusTransaction item)
        {
            var request = new RestRequest
            {
                Resource = "bonuses/update",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };


            request.AddBody(item);

            var response = await _client.ExecuteAsync<BonusTransaction>(request).ConfigureAwait(false);
            return response;
        }
    }
}

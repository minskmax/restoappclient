﻿using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models.Dto;
using RestSharp;
using System.Net;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services.Rest
{
    public class FirebaseService : BaseRestService, IFirebaseService
    {
        public FirebaseService(Interfaces.IRestClient _client) : base(_client) { }

        public async Task<bool> UpdateToken(string token)
        {
            if (string.IsNullOrEmpty(token)) return false;

            var request = new RestRequest
            {
                Resource = "messages/token",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            var tokenUpdate = new FirebaseTokenUpdate { Token = token };
            request.AddBody(tokenUpdate);

            var response = await _client.ExecuteAsync<HttpStatusCode>(request).ConfigureAwait(false);
            return response == HttpStatusCode.OK;
        }
    }
}

﻿using RestoAppClient.Core.Interfaces.Rest;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestoAppClient.Core.Models;
using System.Net;
using RestoAppClient.Core.Models.Dto;

namespace RestoAppClient.Core.Services.Rest
{
    public class ImageService : BaseRestService, IImageService
    {
        public ImageService(Interfaces.IRestClient _client) : base(_client)
        {

        }

        public async Task<int> AttachImageToNews(int newsId, byte[] image)
        {
            var item = new Image
            {
                Id = null,
                Data = image,
                NewsId = newsId,
                Thumbnail = null
            };

            var request = new RestRequest
            {
                Resource = "images",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(item);

            var response = await _client.ExecuteAsync<int>(request,200000).ConfigureAwait(false);
            return response;
        }

        public async Task<bool> DeleteImage(int imageId)
        {
            var request = new RestRequest
            {
                Resource = "images/delete",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(imageId);

            var response = await _client.ExecuteAsync(request).ConfigureAwait(false);
            return response == HttpStatusCode.OK ? true : false;
        }

        public async Task<IEnumerable<ImageInfo>> GetImageIdsForNews(int newsId)
        {
            var request = new RestRequest
            {
                Resource = $"images/listIds?newsId={newsId}",
                RequestFormat = DataFormat.Json
            };

            var response = await _client.ExecuteAsync<List<ImageInfo>>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<ImageInfo> LoadImage(int imageId)
        {
            var request = new RestRequest
            {
                Resource = $"images/get?imageId={imageId}",
                RequestFormat = DataFormat.Json
            };

            var response = await _client.ExecuteAsync<ImageInfo>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<ImageInfo> LoadThumbnail(int imageId)
        {
            var request = new RestRequest
            {
                Resource = $"images/getThumb?imageId={imageId}",
                RequestFormat = DataFormat.Json
            };

            var response = await _client.ExecuteAsync<ImageInfo>(request).ConfigureAwait(false);
            return response;
        }
    }
}

﻿using Newtonsoft.Json;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using RestoAppClient.Core.Models.Dto;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services.Rest
{
    public class NewsService : BaseRestService, INewsService
    {
        public NewsService(Interfaces.IRestClient _client) : base(_client)
        {

        }

        public async Task<IEnumerable<News>> GetNewsAsync(int start, int limit)
        {
            var request = new RestRequest
            {
                Resource = $"news/list?start={start}&limit={limit}",
                RequestFormat = DataFormat.Json
            };

            var response = await _client.ExecuteAsync<List<News>>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<News> AddNewsAsync(News item)
        {
            var request = new RestRequest
            {
                Resource = "news",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(item);

            var response = await _client.ExecuteAsync<News>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<bool> PingAsync()
        {
            try
            {
                var request = new RestRequest
                {
                    Resource = $"news/list?start=0&limit=1",
                    RequestFormat = DataFormat.Json
                };

               var response = await _client.ExecuteAsync<List<News>>(request).ConfigureAwait(false);
               return true;
            }
            catch (Exception e)
            {
                //TODO: Logging
                var s = e.Message;
                return false;
            }   
        }

        public async Task<News> UpdateNewsAsync(News item)
        {
            var request = new RestRequest
            {
                Resource = "news/update",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };
            

            request.AddBody(item);

            var response = await _client.ExecuteAsync<News>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<bool> DeleteNewsAsync(int id)
        {
            var request = new RestRequest
            {
                Resource = "news/delete",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(id);

            var response = await _client.ExecuteAsync<HttpStatusCode>(request).ConfigureAwait(false);
            if (response == HttpStatusCode.OK) return true;
            return false;
        }

        public async Task<int> Count()
        {
                var request = new RestRequest
                {
                    Resource = $"news/count",
                    RequestFormat = DataFormat.Json
                };

                var response = await _client.ExecuteAsync<int>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<bool> TrySetColors(int id, int accentColor, int backColor)
        {
            var request = new RestRequest
            {
                Resource = "news/setcolors",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };
            var colorset = new ColorSet { Id = id, AccentColor = accentColor, BackColor = backColor };
            request.AddBody(colorset);

            var response = await _client.ExecuteAsync<HttpStatusCode>(request).ConfigureAwait(false);
            if (response == HttpStatusCode.OK) return true;
            return false;
        }
    }
}

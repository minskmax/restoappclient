﻿using RestoAppClient.Core.Models;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;
using IRestClient = RestoAppClient.Core.Interfaces.IRestClient;

namespace RestoAppClient.Core.Services.Rest
{
    public class RestSharpClient : IRestClient
    {
        public string BaseUrl { get; }

        private string _token;

        public RestSharpClient(string baseUrl, string key, string secret)
        {
            BaseUrl = baseUrl;
        }

        public void UpdateToken(string token)
        {
            _token = token;
        }

        public async Task<User> RequestCode(string phone)
        {
            var request = new RestRequest
            {
                Resource = "users/register",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(new User { Phone = phone });
            var client = new RestClient
            {
                BaseUrl = new System.Uri(BaseUrl),
                Timeout = 20000
            };
            request.AddHeader("Accept", "application/json");

            var response = await client.ExecuteTaskAsync<User>(request).ConfigureAwait(false);
            return response.Data;
        }

        public async Task<string> RequestToken(string phone, string code)
        {
            var request = new RestRequest
            {
                Resource = "users/auth",
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.AddBody(new UserLogin { Phone = phone, Code = code });
            var client = new RestClient
            {
                BaseUrl = new System.Uri(BaseUrl),
                Timeout = 20000
            };
            request.AddHeader("Accept", "application/json");

            var response = await client.ExecuteTaskAsync<string>(request).ConfigureAwait(false);
            return response.Data;
        }

        public async Task<HttpStatusCode> ExecuteAsync(RestRequest request)
        {
            var client = new RestClient
            {
                BaseUrl = new System.Uri(BaseUrl),
                Timeout = 20000
            };
            request.AddHeader("Accept", "application/json");
            AddTokenHeader(request);

            IRestResponse response = new RestResponse();

            try
            {
                response = await client.ExecuteTaskAsync(request).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                response.ResponseStatus = ResponseStatus.Error;
                response.ErrorMessage = ex.Message;
                response.ErrorException = ex;
            }            

            return response.StatusCode;
        }

        public async Task<T> ExecuteAsync<T>(RestRequest request) where T : new()
        {
            return await ExecuteAsync<T>(request, 20000);
        }

        public async Task<T> ExecuteAsync<T>(RestRequest request, int timeout) where T : new()
        {
            RestClient client = null;

            client = new RestClient
            {
                BaseUrl = new System.Uri(BaseUrl),
                Timeout = timeout
            };


            request.AddHeader("Accept", "application/json");
            AddTokenHeader(request);

            IRestResponse<T> response = new RestResponse<T>();

            try
            {
                response = await (client?.ExecuteTaskAsync<T>(request)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                response.ResponseStatus = ResponseStatus.Error;
                response.ErrorMessage = ex.Message;
                response.ErrorException = ex;
            }


            return response.Data;
        }

        public async Task<bool> СheckAdminToken(string token)
        {
            var testString = "testRole";
            var request = new RestRequest
            {
                Resource = $"users/testRole?test={testString}",
                Method = Method.GET,
                RequestFormat = DataFormat.Json
            };

            var client = new RestClient
            {
                BaseUrl = new System.Uri(BaseUrl),
                Timeout = 20000
            };
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", $"Bearer {token}");

            var response = await client.ExecuteTaskAsync<string>(request).ConfigureAwait(false);
            return response.Data == testString ? true : false;
        }

        private void AddTokenHeader(RestRequest request)
        {
            if (!string.IsNullOrEmpty(_token))
            {
                request.AddHeader("Authorization", $"Bearer {_token}");
            }
        }

        public async Task<bool> CheckToken(string token)
        {
            if (String.IsNullOrEmpty(token))
            {
                return false;
            }

            var testString = "test";
            var request = new RestRequest
            {
                Resource = $"users/test?test={testString}",
                Method = Method.GET,
                RequestFormat = DataFormat.Json
            };

            var client = new RestClient
            {
                BaseUrl = new System.Uri(BaseUrl),
                Timeout = 20000
            };
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", $"Bearer {token}");

            IRestResponse<string> response = new RestResponse<string>();

            try
            {
                response = await client.ExecuteTaskAsync<string>(request).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                response.ResponseStatus = ResponseStatus.Error;
                response.ErrorMessage = ex.Message;
                response.ErrorException = ex;
            }
           
            return response.Data == testString ? true : false;
        }

        public string CurrentToken()
        {
            return _token;
        }
    }
}

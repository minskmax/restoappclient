﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using RestSharp;

namespace RestoAppClient.Core.Services.Rest
{
    public class UserService : BaseRestService, IUserService
    {
        private User _currentUser;

        public UserService(Interfaces.IRestClient client) : base(client)
        {
        }

        public Task<bool> CheckToken(string token)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RequestCode(string phone)
        {
            throw new NotImplementedException();
        }

        public Task<string> RequestToken(string phone, string code)
        {
            throw new NotImplementedException();
        }

        public async Task<User> CurrentUser()
        {
            if (_currentUser != null) return _currentUser;

            var request = new RestRequest
            {
                Resource = $"users/currentUser",
                RequestFormat = DataFormat.Json
            };

            var response = await _client.ExecuteAsync<User>(request).ConfigureAwait(false);
            _currentUser = response;
            return response;
        }

        public async Task<User> UserById(string id)
        {
            var request = new RestRequest
            {
                Resource = $"users/byId?id={id}",
                RequestFormat = DataFormat.Json
            };
            var response = await _client.ExecuteAsync<User>(request).ConfigureAwait(false);
            return response;
        }

        public async Task<User> UpdateCurrentUser()
        {
            _currentUser = null;
            return await CurrentUser();
        }
    }
}

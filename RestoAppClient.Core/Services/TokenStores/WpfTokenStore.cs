﻿using RestoAppClient.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.Services.TokenStores
{
    public class WpfTokenStore : ITokenStore
    {
        public Task<string> LoadTokenAsync()
        {
            string res;
            try
            {
                res = File.ReadAllText("token.txt");
            }
            catch (Exception)
            {
                res = string.Empty;
            }
            return Task.FromResult(res);
        }

        public string LoadToken()
        {
            string res;
            try
            {
                res = File.ReadAllText("token.txt");
            }
            catch (Exception)
            {
                res = string.Empty;
            }
            return res;
        }

        public Task StoreToken(string token)
        {
            File.WriteAllText("token.txt", token);
            return Task.FromResult(0);
        }
    }
}

﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using RestoAppClient.Core.ViewModels.Admin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class AdminViewModel : MvxNavigationViewModel
    {
        private MvxObservableCollection<News> _items;
        public MvxObservableCollection<News> Items
        {
            get { return _items; }

            set
            {
                _items = value;
                RaisePropertyChanged(() => Items);
            }

        }

        private bool _loaded;
        public bool Loaded
        {
            get { return _loaded; }
            set
            {
                _loaded = value;
                RaisePropertyChanged(() => Loaded);
            }
        }

        private string _loadError;
        public string LoadError
        {
            get { return _loadError; }
            set { _loadError = value; RaisePropertyChanged(() => LoadError); }
        }

        private bool _adding;
        public bool Adding
        {
            get { return _adding; }
            set { _adding = value; RaisePropertyChanged(() => Adding); }
        }

        private IMvxAsyncCommand _openCommand;
        public IMvxAsyncCommand OpenCommand
        {
            get
            {
                _openCommand =  new MvxAsyncCommand(() => Open());
                return _openCommand;
            }
        }

        private News _selectedItem;
        public News SelectedItem
        {
            get { return _selectedItem; }
            set {
                _selectedItem = value;
                if (!_adding) OpenCommand.ExecuteAsync().ConfigureAwait(false);
                RaisePropertyChanged(() => SelectedItem); }
        }

        private INewsService _newsService;

        public string Count
        {
            get
            {
                return Items.Count.ToString();
            }
        }

        public MvxCommand ReloadCommand
        {
            get { return new MvxCommand(ReloadData); }
        }

        IMvxAsyncCommand _addCommand;
        public IMvxAsyncCommand AddCommand
        {
            get {
                _addCommand = _addCommand ?? new MvxAsyncCommand(() => AddItem());
                return _addCommand;
            }
        }

        IMvxAsyncCommand _deleteCommand;
        public IMvxAsyncCommand DeleteCommand
        {
            get
            {
                _deleteCommand = _deleteCommand ?? new MvxAsyncCommand(() => DeleteItem());
                return _deleteCommand;
            }
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            Task.Run(async () => await PingAndLoadData().ConfigureAwait(false)).ConfigureAwait(false);            
        }

        public AdminViewModel(INewsService service, IMvxLogProvider logger, IMvxNavigationService navigation):base(logger,navigation)
        {
            _newsService = service;
            _items = new MvxObservableCollection<News>();
           
        }

        public AdminViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
        }

        public async void ReloadData()
        {
            await LoadData().ConfigureAwait(false);
        }

        private async Task DeleteItem()
        {
            Adding = true;
            Loaded = false;
            if (SelectedItem?.Id != null)
            {
                var result = await _newsService.DeleteNewsAsync((int)_selectedItem.Id).ConfigureAwait(false);
                Adding = false;
                await InvokeOnMainThreadAsync(() => { Items.Remove(SelectedItem);}).ConfigureAwait(false);
            }
            Adding = false;
            Loaded = true;
        }

        private async Task AddItem()
        {
            Adding = true;

            var item = new News
            {
                Created = DateTime.Now
            };

            await InvokeOnMainThreadAsync(() => _items.Insert(0,item)).ConfigureAwait(false);
            SelectedItem = item;
            var result = await NavigationService.Navigate<DetailViewModel, News, News>(_selectedItem).ConfigureAwait(false);
            if (result==null)
            {
                Items.Remove(item);
            }
            else
            {
                await InvokeOnMainThreadAsync(() => {
                                    Items.Remove(item);
                    Items.Insert(0, result);
                    }).ConfigureAwait(false);
                await RaisePropertyChanged(()=>Items).ConfigureAwait(false);
            }
            Adding = false;
            if (result!=null)
            {
                SelectedItem = result;
            }
        }

        private async Task CancelAddItem()
        {
            await InvokeOnMainThreadAsync(() => _items.Remove(_selectedItem)).ConfigureAwait(false);
        }

        private async Task PingAndLoadData()
        {
            //await _newsService.PingAsync().ConfigureAwait(false); TODO: make ping request to server simple with returning status code
            await LoadData().ConfigureAwait(false);
        }

        private async Task LoadData()
        {
            LoadError = string.Empty;
            Loaded = false;
            try
            {
                var listOfNews = await _newsService.GetNewsAsync(0, 100).ConfigureAwait(false);
                if (listOfNews == null) throw new ApplicationException("Не удалось загрузить данные. Проверьте подключение к интернету.");
                foreach (var item in listOfNews)
                {
                    item.Created = DateTime.SpecifyKind(item.Created, DateTimeKind.Utc);
                    await InvokeOnMainThreadAsync(() => _items.Add(item)).ConfigureAwait(false);
                }
                Loaded = true;
            }
            catch (Exception e)
            {
                Loaded = false;
                LoadError = e.Message;
            }        
        }

        private async Task<News> Open()
        {
            var item = _selectedItem;

            var result = await NavigationService.Navigate<DetailViewModel, News, News>(_selectedItem).ConfigureAwait(false);
            if (result!=null)
            {
                var itemIndex = Items.IndexOf(item);
                await InvokeOnMainThreadAsync(() => {
                    Items.Remove(item);
                    if (Items.Count > 0) Items.Insert(itemIndex, result);
                    else Items.Add(result);
                }).ConfigureAwait(false);
                await RaisePropertyChanged(() => Items).ConfigureAwait(false);
                SelectedItem = result;
            }
            //await RaisePropertyChanged(() => SelectedItem).ConfigureAwait(false);
            return result;
        }
    }
}

﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Admin
{
    public class AuthViewModel : MvxNavigationViewModel<string>
    {
        private IRestClient _service;
        private ITokenStore _tokenStore;

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                RaisePropertyChanged(() => Phone);
            }
        }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                RaisePropertyChanged(() => Code
);
            }
        }

        private IMvxAsyncCommand _requestTokenCommand;
        public IMvxAsyncCommand RequestTokenCommand
        {
            get
            {
                _requestTokenCommand = new MvxAsyncCommand(() => RequestTokenAsync());
                return _requestTokenCommand;
            }
        }

        public AuthViewModel(ITokenStore store,IRestClient service, IMvxLogProvider logger, IMvxNavigationService navigation) : base(logger, navigation)
        {
            _service = service;
            _tokenStore = store;
        }

        private async Task RequestTokenAsync()
        {
            var result = await _service.RequestToken(_phone,_code).ConfigureAwait(false);
            if (!string.IsNullOrEmpty(result) && await _service.СheckAdminToken(result))
            {
                await _tokenStore.StoreToken(result);
                _service.UpdateToken(result);
                await NavigationService.Navigate<AdminViewModel>();
            }
            else
            {
                await NavigationService.Navigate<LoginViewModel>();
            }
        }

        public override void Prepare(string parameter)
        {
            Phone = parameter;
        }
    }
}

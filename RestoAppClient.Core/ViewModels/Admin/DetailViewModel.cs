﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Admin
{
    public class DetailViewModel : MvxViewModel<News, News>
    {
        private bool _closeAfterAppear;

        private IMvxNavigationService _navigation;
        private INewsService _newsService;
        private IImageService _imageService;
        private IFileDialogService _fileDialog;
        private IImageReader _imageReader;
        private IPictureColors _pictureColors;

        public MvxObservableCollection<ThumbViewModel> Thumbs
        {
            get { return _thumbnails; }

            set
            {
                _thumbnails = value;
                RaisePropertyChanged(() => Thumbs);
            }

        }
        private MvxObservableCollection<ThumbViewModel> _thumbnails;

        private News _news;
        private News _oldNews;

        public int? Id { get { return _news.Id; } set { _news.Id = value; Validate(); RaisePropertyChanged(() => Id); } }
        public string Header { get { return _news.Header; } set { _news.Header = value; Validate(); RaisePropertyChanged(() => Header); } }
        public string Body
        {
            get { return _news.Body; }
            set
            {
                _news.Body = value; Validate();
                RaisePropertyChanged(() => Body);
            }
        }
        public bool Push { get { return _news.Push; } set { _news.Push = value; Validate(); RaisePropertyChanged(() => Push); } }
        public DateTime Created { get { return _news.Created; } set { _news.Created = value; Validate(); RaisePropertyChanged(() => Created); } }

        private string _error;
        public string ValidationError { get { return _error; } set { _error = value; RaisePropertyChanged(() => ValidationError); } }

        public bool IsValid
        {
            get
            {
                return Validate();
            }
        }

        private bool _loading;

        public bool Loading
        {
            get { return _loading; }
            set { _loading = value; RaisePropertyChanged(() => Loading); }
        }


        public MvxCommand CancelChanges
        {
            get { return new MvxCommand(Cancel); }
        }

        IMvxAsyncCommand _changeCommand;
        public IMvxAsyncCommand ChangeCommand
        {
            get
            {
                _changeCommand = _changeCommand ?? new MvxAsyncCommand(() => Post());
                return _changeCommand;
            }
        }

        IMvxAsyncCommand _addImageCommand;
        public IMvxAsyncCommand AddImageCommand
        {
            get
            {
                _addImageCommand = _addImageCommand ?? new MvxAsyncCommand(() => AddImage());
                return _addImageCommand;
            }
        }

        public DetailViewModel(IImageReader imageReader,IFileDialogService fileService,IMvxNavigationService navService, INewsService newsService, IImageService imageService, IPictureColors pictureColors)
        {
            _navigation = navService;
            _newsService = newsService;
            _imageService = imageService;
            _fileDialog = fileService;
            _imageReader = imageReader;
            _pictureColors = pictureColors;
            _thumbnails = new MvxObservableCollection<ThumbViewModel>();
        }

        private void Cancel()
        {
            _navigation.Close(this, null);
        }

        private bool Validate()
        {
            var oldVal = ValidationError;
            if (_news.Header is null || Header.Length < 10) { ValidationError = "Слишком короткий заголовок"; return false; }
            if (_news.Header.Length > 100) { ValidationError = "Слишком длинный заголовок"; return false; }
            if (_news.Body is null || Body.Length < 10) { ValidationError = "Слишком короткий текст"; return false; }
            if (_news.Body.Length > 3000) { ValidationError = "Слишком длинный текст"; return false; }
            if (!ChangeCheck()) { ValidationError = "Нет изменений"; return false; }
            ValidationError = string.Empty;
            if (ValidationError != oldVal) RaisePropertyChanged(() => IsValid);
            return true;
        }

        private bool ChangeCheck()
        {
            var ch = false;
            if (Header != _oldNews.Header) ch = true;
            if (Body != _oldNews.Body) ch = true;
            if (Created != _oldNews.Created) ch = true;
            return ch;
        }

        public override void ViewAppeared()
        {
            if (_closeAfterAppear) _navigation.Close(this);
            Task.Run(async () => await CreateThumbs().ConfigureAwait(false)).ConfigureAwait(false);
        }

        public override void Prepare(News parameter)
        {
            _news = new News();
            if (parameter is null)
            {
                _closeAfterAppear = true;
                return;
            }
            _oldNews = parameter;
            Id = parameter.Id;
            Header = parameter.Header;
            Body = parameter.Body;
            Push = parameter.Push;
            Created = parameter.Created;
        }

        private async Task CreateThumbs()
        {
            if (Id != null)
            {
                var ids = await _imageService.GetImageIdsForNews((int)Id).ConfigureAwait(false);
                if (ids!=null)
                {
                    if (_thumbnails.Count > 0)
                    {
                        InvokeOnMainThread(() => _thumbnails.Clear());
                    }

                    foreach (var id in ids)
                    {
                        var thumb = new ThumbViewModel(_imageService, id.ImageId, Thumbs);
                        await InvokeOnMainThreadAsync(()=>_thumbnails.Add(thumb)).ConfigureAwait(false);
                    }
                }
                await RaisePropertyChanged(()=>Thumbs);
            }
        }

        private async Task Post() //posting to server, if id == null then add, overwise update
        {
            Loading = true;
            if (_news.Id == null) await PostAdd();
            else await PostUpdate();
            Loading = false;
        }

        private async Task PostAdd()
        {
            if (IsValid)
            {
                var result = await _newsService.AddNewsAsync(_news);
                if (result != null)
                {
                    _news.Id = result.Id;//change id of edited item with newly created on server TODO:remove?
                    await _navigation.Close(this, result);
                }
                else throw new ApplicationException("network error");
            }
        }

        private async Task PostUpdate()
        {
            if (IsValid)
            {
                var result = await _newsService.UpdateNewsAsync(_news);
                if (result != null)
                {
                    await _navigation.Close(this, result);
                }
                else throw new ApplicationException("network error");
            }
        }

        private async Task AddImage()
        {
            ThumbViewModel newThumb;
            if (_fileDialog.OpenFileDialog())
            {
                var added = false;
                newThumb = new ThumbViewModel(_imageService,-1,Thumbs);
                await InvokeOnMainThreadAsync(()=>_thumbnails.Add(newThumb)).ConfigureAwait(false);
                await RaisePropertyChanged(() => Thumbs);
                var data = await _imageReader.LoadImage(_fileDialog.FilePath).ConfigureAwait(false);
                if (data != null && data.Length > 0 && Id != null && Id >= 0)
                {
                    var imageId = await _imageService.AttachImageToNews((int)Id, data).ConfigureAwait(false);
                    await CreateThumbs();
                    added = true;
                }
                else
                {
                   await InvokeOnMainThreadAsync(() => _thumbnails.Remove(newThumb)).ConfigureAwait(false);
                   added = false;
                }
                await RaisePropertyChanged(() => Thumbs);
                if (added)
                {
                    var colors = _pictureColors.GetThumbColors(data);
                    if (colors.Length==2)
                    {
                        await _newsService.TrySetColors((int)Id, colors[0].color, colors[1].color).ConfigureAwait(false);
                    }
                }
            }
        }
    }
}

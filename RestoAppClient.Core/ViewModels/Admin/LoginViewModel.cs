﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Admin
{
    public class LoginViewModel : MvxNavigationViewModel
    {
        private IRestClient _service;

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                RaisePropertyChanged(() => Phone);
            }
        }

        private IMvxAsyncCommand _requestCodeCommand;
        public IMvxAsyncCommand RequestCodeCommand
        {
            get
            {
                _requestCodeCommand = new MvxAsyncCommand(() => RequestCodeAsync());
                return _requestCodeCommand;
            }
        }

        public LoginViewModel(IRestClient service, IMvxLogProvider logger, IMvxNavigationService navigation) : base(logger, navigation)
        {
            _service = service;
        }

        public override void ViewAppeared()
        {
            
        }

        private async Task RequestCodeAsync()
        {
            var result = await _service.RequestCode(_phone).ConfigureAwait(false);
            if (result != null) await NavigationService.Navigate<AuthViewModel,string>(_phone);
        }
    }
}

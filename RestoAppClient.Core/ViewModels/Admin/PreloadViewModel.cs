﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Admin
{
    public class PreloadViewModel : MvxNavigationViewModel
    {
        private IRestClient _service;
        private ITokenStore _tokenStore;

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                RaisePropertyChanged(() => Phone);
            }
        }

        private IMvxAsyncCommand _requestCodeCommand;
        public IMvxAsyncCommand RequestCodeCommand
        {
            get
            {
                _requestCodeCommand = new MvxAsyncCommand(() => RequestCodeAsync());
                return _requestCodeCommand;
            }
        }

        public PreloadViewModel(ITokenStore tokenStore, IRestClient service, IMvxLogProvider logger, IMvxNavigationService navigation) : base(logger, navigation)
        {
            _service = service;
            _tokenStore = tokenStore;
        }

        public override void ViewAppeared()
        {
            Task.Run(async () => await CheckToken().ConfigureAwait(false));
        }

        

        private async Task<bool> CheckToken()
        {
            var token = await _tokenStore.LoadTokenAsync();

            if (!string.IsNullOrEmpty(token))
            {
                var tokenCorrect = await _service.СheckAdminToken(token);
                if (tokenCorrect)
                {
                    _service.UpdateToken(token);
                    await NavigationService.Navigate<AdminViewModel>();
                    return true;
                }
            }
            await NavigationService.Navigate<LoginViewModel>();
            return false;
        }

        private async Task RequestCodeAsync()
        {
            var result = await _service.RequestCode(_phone).ConfigureAwait(false);
            if (result != null) await NavigationService.Navigate<AuthViewModel, string>(_phone);
        }
    }
}

﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Rest;
using System;
using System.Collections;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Admin
{
    public class ThumbViewModel : MvxViewModel
    {
        private IImageService _service;
        private byte[] _thumb;
        private MvxObservableCollection<ThumbViewModel> _owner;

        public byte[] Thumb
        {
            get { return _thumb; }
            private set { _thumb = value; RaisePropertyChanged(() => Thumb); }
        }
        private int _id;
        public int Id
        {
            get { return _id; }
            private set { _id = value; RaisePropertyChanged(() => Id); }
        }

        IMvxAsyncCommand _deleteCommand;
        public IMvxAsyncCommand DeleteCommand
        {
            get
            {
                _deleteCommand = _deleteCommand ?? new MvxAsyncCommand(async () => await Delete());
                return _deleteCommand;
            }
        }

        public ThumbViewModel(IImageService service, int id, MvxObservableCollection<ThumbViewModel> owner)
        {
            _service = service;
            _owner = owner;
            _id = id;
            Thumb = null;
            if (id != -1)
            {
                Task.Run(async () => await LoadThumb().ConfigureAwait(false)).ConfigureAwait(false);
            }
        }

        private async Task LoadThumb()
        {
            Thumb = Convert.FromBase64String((await _service.LoadThumbnail(_id).ConfigureAwait(false)).Data);
        }

        private async Task Delete()
        {
            await InvokeOnMainThreadAsync(() => _owner.Remove(this)).ConfigureAwait(false);
            if (!(await _service.DeleteImage(_id).ConfigureAwait(false)))
            {
                await InvokeOnMainThreadAsync(() => _owner.Add(this)).ConfigureAwait(false);
                await RaisePropertyChanged(()=>_owner);
            }
        }
    }
}

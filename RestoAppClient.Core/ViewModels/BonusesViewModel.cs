﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Exceptions;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class BonusesViewModel : MvxNavigationViewModel
    {
        private IQrCodeGenerator _qrGenerator;
        private IUserService _userService;
        private IBonusesService _bonusesService;

        public BonusesViewModel(IQrCodeGenerator qrGenerator, IBonusesService bonusService, IUserService service, IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
            _userService = service;
            _qrGenerator = qrGenerator;
            _bonusesService = bonusService;
        }

        private MvxCommand _showCode;
        public MvxCommand ShowCode
        {
            get
            {
                _showCode = _showCode ?? new MvxCommand(() => NavigationService.Navigate<QrCodeViewModel>());                
                return _showCode;
            }
        }

        private object _qrCode;
        public object QrCode
        {
            get { return _qrCode; }
            set { _qrCode = value; RaisePropertyChanged(() => QrCode); }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; RaisePropertyChanged(() => UserName); }
        }

        private string _userPhone;
        public string UserPhone
        {
            get { return _userPhone; }
            set { _userPhone = value; RaisePropertyChanged(() => UserPhone); }
        }

        private string _userLevel;
        public string UserLevel
        {
            get { return _userLevel; }
            set { _userLevel = value; RaisePropertyChanged(() => UserLevel); }
        }

        private string _userLevelColor;
        public string UserLevelColor
        {
            get { return _userLevelColor; }
            set { _userLevelColor = value; RaisePropertyChanged(() => UserLevelColor); }
        }

        private string _userBonuses;
        public string UserBonuses
        {
            get { return _userBonuses; }
            set { _userBonuses = value; RaisePropertyChanged(() => UserBonuses); }
        }

        private int _userTotalBonuses;
        public int UserTotalBonuses
        {
            get { return _userTotalBonuses; }
            set { _userTotalBonuses = value; RaisePropertyChanged(() => UserTotalBonuses); }
        }

        private string _activeBonuses;
        public string ActiveBonuses
        {
            get { return _activeBonuses; }
            set { _activeBonuses = value;RaisePropertyChanged(() => ActiveBonuses);}
        }


        public override async Task Initialize()
        {
            await base.Initialize();
            await PrepareData();
        }

        public override void ViewAppeared()
        {
            //if (!_authService.IsAuthorized) NavigationService.Close(this);
        }

        private async Task PrepareData()
        {
            _qrCode = _qrGenerator.GenerateQrCode("http://sinebruhof.ru",50);

            var user = await _userService.UpdateCurrentUser(); ;

            if (user == null)
            {
                user = await _userService.UpdateCurrentUser();
                if (user==null)
                    throw new ConnectionErrorException();
            }

            QrCode = _qrGenerator.GenerateQrCode(user.Phone);
            UserPhone = user.Phone;

            if (user.Bonuses > 0)
            {
                UserBonuses = user.Bonuses.ToString();
            }            

            UserTotalBonuses = user.TotalBonuses;
            UserLevel = user.LevelName;
            UserLevelColor = user.LevelColor;

            var inactiveBonuses = await _bonusesService.GetInactiveBonuses(user.Id);
            if (inactiveBonuses>0 && !string.IsNullOrEmpty(UserBonuses))
            {
                int bonuses = int.Parse(UserBonuses);
                ActiveBonuses = (bonuses - inactiveBonuses).ToString();
            }
        }
    }
}

﻿using MvvmCross;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Collections;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Items
{
    public class ImageItem : MvxViewModel,ICollectionItem
    {
        private IImageService _imageService;
        //private IBitmapConverter _converter;
        public ImageItem(IImageService service, int? id=null)
        {
            _imageService = service;
            _id = id;
            //_converter = converter;
            //_nativeBitmap = _converter.NativeBlankImage;
            //Task.Run(async () => await LoadImage().ConfigureAwait(false)).ConfigureAwait(false); TODO: inspect this. needed?
        }

        public ImageItem()
        {
            _imageService = Mvx.IoCProvider.Resolve<IImageService>();
            //_converter = Mvx.IoCProvider.Resolve<IBitmapConverter>();
            //_nativeBitmap = _converter.NativeBlankImage;
        }

        private bool _loaded;
        public bool Loaded
        {
            get { return _loaded; }
            set { _loaded = value;RaisePropertyChanged(() => Loaded); }
        }

        private byte[] _image;
        public byte[] Image
        {
            get { return _image; }
            set { _image = value; RaisePropertyChanged(() => Image); }
        }

        private int? _id;
        public int? Id
        {
            get { return _id; }
            private set { _id = value; RaisePropertyChanged(() => Id); }
        }

        private int _newsId;
        public int NewsId
        {
            get { return _newsId; }
            set { _newsId = value; RaisePropertyChanged(() => NewsId); }
        }

        //private object _nativeBitmap;

        //public object NativeBitmap
        //{
        //    get { return _nativeBitmap; }
        //    set { _nativeBitmap = value;RaisePropertyChanged(() => NativeBitmap); }
        //}


        public async Task LoadImage()
        {
            if (_image == null && _id!=null)
            {
                var img = (await _imageService.LoadImage((int)_id).ConfigureAwait(false))?.Data;
                if (img != null)
                {
                    _image = Convert.FromBase64String(img);
                    //NativeBitmap = await _converter.FromByteArray(_image,_nativeBitmap);
                    Loaded = true;
                }
                await RaisePropertyChanged(() => Image);
            }
        }

        //public async Task RefreshImage()
        //{
        //    if (_image != null)
        //    {
        //            //NativeBitmap = await _converter.FromByteArray(_image,_nativeBitmap);            
        //    }
        //}
    }
}

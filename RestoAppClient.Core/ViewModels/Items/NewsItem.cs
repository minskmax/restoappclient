﻿using AutoMapper;
using MvvmCross;
using MvvmCross.UI;
using MvvmCross.ViewModels;
using Newtonsoft.Json;
using RestoAppClient.Core.Interfaces.Collections;
using RestoAppClient.Core.Interfaces.Rest;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Items
{
    public class NewsItem : MvxViewModel, ICollectionItem
    {
        private readonly IImageService _imageService;

        private int? _id;
        public int? Id
        {
            get { return _id; }
            set { _id = value; RaisePropertyChanged(() => Id); }
        }


        private string _header;
        public string Header
        {
            get { return _header; }
            set { _header = value; RaisePropertyChanged(() => Header); }
        }

        private string _body;
        public string Body
        {
            get { return _body; }
            set
            {
                _body = value; RaisePropertyChanged(() => Body
);
            }
        }

        private DateTime _created;
        public DateTime Created
        {
            get { return _created; }
            set { _created = value; RaisePropertyChanged(() => Created); }
        }

        private DateTime _modified;
        public DateTime Modified
        {
            get { return _modified; }
            set { _modified = value; RaisePropertyChanged(() => Modified); }
        }

        private int? _accentColor;
        public int? AccentColor
        {
            get { return _accentColor; }
            set { _accentColor = value; RaisePropertyChanged(() => AccentColor); }
        }

        private int? _backColor;
        public int? BackColor
        {
            get { return _backColor; }
            set { _backColor = value; RaisePropertyChanged(() => BackColor); }
        }

        private bool _imagesLoaded;

        public bool ImagesLoaded
        {
            get { return _imagesLoaded; }
            set { _imagesLoaded = value; RaisePropertyChanged(() => ImagesLoaded); }
        }


        private MvxObservableCollection<ImageItem> _images;
        public MvxObservableCollection<ImageItem> Images
        {
            get { return _images; }
            set { _images = value; RaisePropertyChanged(() => Images); }
        }

        public NewsItem(IImageService imageService, int? id):this(imageService,id,false)
        {
            
        }

        public NewsItem(IImageService imageService,int? id,bool cached)
        {
            Id = id;

            _imageService = imageService;
            _images = new MvxObservableCollection<ImageItem>();
            //add blank image if not creating from cache
            if (!cached)
            {
                _images.Add(Mvx.IoCProvider.IoCConstruct<ImageItem>());
            }

            // Task.Run(async () => await LoadImages().ConfigureAwait(false)).ConfigureAwait(false);
        }

        public async Task LoadImages()
        {
            if (Id != null)
            {
                var ids = await _imageService.GetImageIdsForNews((int)Id).ConfigureAwait(false);
                if (ids != null)
                {
                    foreach (var item in ids)
                    {
                        /* await InvokeOnMainThreadAsync(() => */
                        Images.Add(Mapper.Map<ImageItem>(item));
                        /*)).ConfigureAwait(false);*/
                    }
                    ImagesLoaded = true;
                    //delete fake images
                    var fakeImages = Images.Where(x => x.Id == null).ToList();
                    if (fakeImages != null && fakeImages.Count > 0)
                    {
                        /*await InvokeOnMainThreadAsync(() => */
                        Images.RemoveItems(Images.Where(x => x.Id == null).ToList());
                    }

                    await RaisePropertyChanged(() => Images);

                    foreach (var item in Images)
                    {
                        if (!item.Loaded)
                        {
                            await item.LoadImage();
                        }
                    }
                    await RaisePropertyChanged(() => Images);
                }
            }
        }

        public string SerializeToJson()
        {            
            var result = JsonConvert.SerializeObject(this);
            return result;
        }        

        public static NewsItem FromJson(string data)
        {
            var newItem = new NewsItem(Mvx.IoCProvider.Resolve<IImageService>(), null, true);
            try
            {
                JsonConvert.PopulateObject(data, newItem);
            }
            catch (Exception e)
            {
                var tmp = e.Message;
            }            
            return newItem;
        }
    }
}

﻿using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Collections;
using System;

namespace RestoAppClient.Core.ViewModels.Items
{
    public class TransactionItem : MvxViewModel, ICollectionItem
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; RaisePropertyChanged(() => Id); }
        }

        private string _ownerId;
        public string OwnerId
        {
            get => _ownerId;
            set { _ownerId = value; RaisePropertyChanged(OwnerId); }
        }

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; RaisePropertyChanged(() => Phone); }
        }

        private int _count;
        public int Count
        {
            get { return _count; }
            set { _count = value; RaisePropertyChanged(() => Count); }
        }

        private string _cause;
        public string Cause
        {
            get { return _cause; }
            set { _cause = value; RaisePropertyChanged(() => Cause); }
        }

        private DateTime _date;
        public DateTime Date
        {
            get => _date;
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
                RaisePropertyChanged(() => DateFormatted);
            }
        }

        public string DateFormatted => _date.ToLocalTime().ToShortDateString();        

        private string _creatorId;
        public string CreatorId
        {
            get => _creatorId;
            set
            {
                _creatorId = value;
                RaisePropertyChanged(() => CreatorId);
            }
        }
    }
}

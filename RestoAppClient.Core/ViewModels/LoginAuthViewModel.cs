﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels.Admin
{
    public class LoginAuthViewModel : MvxNavigationViewModel<IMvxViewModel,bool>
    {
        private IRestClient _service;
        private IFirebaseService _firebase;
        private ITokenAccess _token;

        private IMvxViewModel _viewToBackTo;

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set
            {
                _phone = value;
                RaisePropertyChanged(() => Phone);
            }
        }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                RaisePropertyChanged(() => Code);
            }
        }

        private bool _codeRequested;        
        public bool CodeRequested
        {
            get { return _codeRequested; }
            set { _codeRequested = value;RaisePropertyChanged(() => CodeRequested); }
        }

        private string _error;

        public string ErrorMessage
        {
            get { return _error; }
            set { _error = value;RaisePropertyChanged(() => ErrorMessage); }
        }





        private ITokenStore _tokenStore;

        private IMvxAsyncCommand _requestCodeCommand;
        public IMvxAsyncCommand RequestCodeCommand
        {
            get
            {
                _requestCodeCommand = new MvxAsyncCommand(() => RequestCodeAsync());
                return _requestCodeCommand;
            }
        }

        private IMvxAsyncCommand _requestTokenCommand;
        public IMvxAsyncCommand RequestTokenCommand
        {
            get
            {
                _requestTokenCommand = new MvxAsyncCommand(() => RequestTokenAsync());
                return _requestTokenCommand;
            }
        }

        public LoginAuthViewModel(ITokenStore store, IRestClient service, IMvxLogProvider logger, IMvxNavigationService navigation, ITokenAccess token, IFirebaseService firebase) : base(logger, navigation)
        {
            _service = service;
            _tokenStore = store;
            _token = token;
            _firebase = firebase;
        }

        private async Task RequestCodeAsync()
        {
            var result = await _service.RequestCode(_phone).ConfigureAwait(false);
            if (result != null)
            {
                CodeRequested = true;
            }
        }

        private async Task RequestTokenAsync()
        {
            var result = await _service.RequestToken(_phone, _code).ConfigureAwait(false);
            if (!string.IsNullOrEmpty(result) && (await _service.CheckToken(result).ConfigureAwait(false)))
            {
                await _tokenStore.StoreToken(result);
                _service.UpdateToken(result);
                var token = _token.GetFirebaseToken();
                if (!string.IsNullOrEmpty(token))
                {
                    await _firebase.UpdateToken(token).ConfigureAwait(false);
                }

                await NavigationService.Close(this, true);
            }
            else
            {
                CodeRequested = false;
            }
        }

        public override void Prepare(IMvxViewModel parameter)
        {
            _viewToBackTo = parameter;
        }
    }
}

﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.CommonTypes;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.ViewModels.Admin;
using System;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class MainContainerViewModel : MvxNavigationViewModel
    {
        private IAuthorizationService _authService;

        private bool _isAdmin;
        public bool IsAdmin
        {
            get { return _isAdmin; }
            private set
            {
                _isAdmin = value;
                RaisePropertyChanged(() => IsAdmin);
            }
        }

        public event EventHandler<bool> AdminChanged;

        public MainContainerViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IAuthorizationService authService) : base(logProvider, navigationService)
        {
            _authService = authService;
        }

        public override async Task Initialize()
        {
            if (await _authService.Authorize() == AuthResult.Success)
            {
                AdminCheck();
            }            
        }

        IMvxAsyncCommand _newsCommand;
        public IMvxAsyncCommand NavigateToNewsCommand
        {
            get
            {
                _newsCommand = _newsCommand ?? new MvxAsyncCommand(() => NavigationService.Navigate<NewsListViewModel>());
                return _newsCommand;
            }
        }

        IMvxAsyncCommand _bonusesCommand;
        public IMvxAsyncCommand NavigateToBonusesCommand
        {
            get
            {
                _bonusesCommand = _bonusesCommand ?? new MvxAsyncCommand(() => Auth<BonusesViewModel>());
                //new MvxAsyncCommand(()=> LoginCheck());
                return _bonusesCommand;
            }
        }

        IMvxAsyncCommand _adminCommand;
        public IMvxAsyncCommand NavigateToAdminCommand
        {
            get
            {
                _adminCommand = _adminCommand ?? new MvxAsyncCommand(() => NavigationService.Navigate<MobileAdminViewModel>());
                return _adminCommand;
            }
        }

        IMvxAsyncCommand _contactsCommand;
        public IMvxAsyncCommand NavigateToContactsCommand
        {
            get
            {
                _contactsCommand = _contactsCommand ?? new MvxAsyncCommand(() => NavigationService.Navigate<ContactsViewModel>());
                return _contactsCommand;
            }
        }

        private bool _loading;

        public bool Loading
        {
            get { return _loading; }
            set { _loading = value;RaisePropertyChanged(() => Loading); }
        }


        private async Task Auth<T>() where T : IMvxViewModel
        {
            Loading = true;
            if ((await _authService.Authorize().ConfigureAwait(false)) == AuthResult.Success) //On authorization succes navigate to target viewmodel
            {
                AdminCheck();
                Loading = false;
                await NavigationService.Navigate<T>().ConfigureAwait(false);
            }
            else //On authorisation unsuccess navigate to LoginAuthViewModel for login
            {
                Loading = false;
                var authResult = await NavigationService.Navigate<LoginAuthViewModel, bool>(); //If login form success authorize and navigate to target
                if (authResult)
                {
                    Loading = true;
                    if (await _authService.Authorize().ConfigureAwait(false) == AuthResult.Success)
                    {
                        AdminCheck();
                        Loading = false;
                        await NavigationService.Navigate<T>().ConfigureAwait(false);                        
                    }
                }
            }
            Loading = false;
        }

        private void AdminCheck()
        {
            if (_authService.IsAdmin != IsAdmin)
            {
                IsAdmin = _authService.IsAdmin;
                AdminChanged?.Invoke(this, IsAdmin);
            }
        }
    }
}

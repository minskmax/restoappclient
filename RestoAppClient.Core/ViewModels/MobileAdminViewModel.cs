﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models.Dto;
using RestoAppClient.Core.Services.Collections;
using RestoAppClient.Core.ViewModels.Items;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class MobileAdminViewModel : MvxNavigationViewModel
    {

        private TransactionsCollection _transactions;
        private IBonusesService _bonusService;

        public TransactionsCollection Transactions
        {
            get { return _transactions; }
        }

        IMvxCommand _resetPhone;
        public IMvxCommand ResetPhoneCommand
        {
            get
            {
                _resetPhone = _resetPhone ?? new MvxAsyncCommand(() => UpdatePhone(string.Empty));
                return _resetPhone;
            }
        }

        IMvxAsyncCommand _addBonuses;
        public IMvxAsyncCommand AddBonusesCommand
        {
            get
            {
                _addBonuses = _addBonuses ?? new MvxAsyncCommand(async () =>
                {
                    await AddBonuses();
                });
                return _addBonuses;
            }
        }        

        IMvxAsyncCommand _minusBonuses;
        public IMvxAsyncCommand RemoveBonusesCommand
        {
            get
            {
                _minusBonuses = _minusBonuses ?? new MvxAsyncCommand(async () =>
                {
                    await SubBonuses();
                });

                return _minusBonuses;
            }
        }

        private IMvxAsyncCommand<TransactionItem> _transactionSelectedCommand;
        public IMvxAsyncCommand<TransactionItem> TransactionSelectedCommand
        {
            get { return _transactionSelectedCommand ?? (_transactionSelectedCommand = new MvxAsyncCommand<TransactionItem>(OnTransactionSelected)); }
        }

        private async Task OnTransactionSelected(TransactionItem selectedTransaction)
        {
            await NavigationService.Navigate<TransactionDetailsViewModel, TransactionItem>(selectedTransaction);
        }

        public string SelectedPhone
        {
            get { return _transactions.Phone; }
            set { _transactions.Phone = value; RaisePropertyChanged(() => SelectedPhone); }
        }

        private string _errorMessage;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; RaisePropertyChanged(() => ErrorMessage); }
        }



        public MobileAdminViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBonusesService service, TransactionsCollection collection) : base(logProvider, navigationService)
        {
            _transactions = collection;
            _bonusService = service;
        }

        public async override Task Initialize()
        {
            await UpdatePhone(string.Empty);
        }

        public async Task UpdatePhone(string phone)
        {
            SelectedPhone = phone;
            await _transactions.Reload().ConfigureAwait(false);
        }

        private async Task AddBonuses()
        {
            var result = await NavigationService.Navigate<TransactionPlusViewModel, TransactionArgs, bool>(new TransactionArgs(_transactions.Phone));
            if (result) await UpdatePhone(SelectedPhone);
        }

        private async Task SubBonuses()
        {
            var result = await NavigationService.Navigate<TransactionMinusViewModel, TransactionArgs, bool>(new TransactionArgs(_transactions.Phone));
            if (result) await UpdatePhone(SelectedPhone);
        }
    }
}

﻿using MvvmCross;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using RestoAppClient.Core.Services.Collections;
using RestoAppClient.Core.ViewModels.Items;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class NewsListViewModel : MvxNavigationViewModel
    {
        private INewsService _newsService;
        private IImageService _imageService;

        private NewsCollection _news;
        public NewsCollection News
        {
            get { return _news; }
            set { _news = value; RaisePropertyChanged(() => News); }
        }
        

        public bool Loading { get => News.Loading; set { RaisePropertyChanged(() => Loading); } }

        public event EventHandler NeedScrollToTop;
        private void OnNeedScrollToTop(object sender,EventArgs args)
        {
            NeedScrollToTop?.Invoke(this,EventArgs.Empty);
        }

        public NewsListViewModel(INewsService newsService, IImageService imageService, IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
            _newsService = newsService;
            _imageService = imageService;            
            _news = Mvx.IoCProvider.Resolve<NewsCollection>();
            RaisePropertyChanged(()=>News);
            _news.LoadingChanged += OnLoadingChanged;
            _news.NeedUpdate += OnNeedUpdate;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            if (_news.Count < 1)
            {
                await _news.LoadCache().ConfigureAwait(false);
            }
            await _news.RefreshData().ConfigureAwait(false);
        }

        protected override void SaveStateToBundle(IMvxBundle bundle)
        {
            base.SaveStateToBundle(bundle);

        }

        protected override void ReloadFromBundle(IMvxBundle state)
        {
            base.ReloadFromBundle(state);
        }

        private void OnLoadingChanged(object sender, EventArgs args)
        {            
            Loading = News.Loading;
        }

        private void OnNeedUpdate(object sender, EventArgs args)
        {
            OnNeedScrollToTop(this, args);
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            //_news = null;
            NeedScrollToTop = null;
            GC.Collect();
            _news.LoadingChanged -= OnLoadingChanged;
            _news.NeedUpdate -= OnNeedUpdate;

            base.ViewDestroy(viewFinishing);
        }
    }
}

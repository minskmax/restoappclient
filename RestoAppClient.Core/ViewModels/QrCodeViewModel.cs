﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Core.Interfaces.Rest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class QrCodeViewModel:MvxNavigationViewModel
    {
        private IQrCodeGenerator _qrGenerator;
        private IUserService _userService;
        private string _userPhone;
        

        public QrCodeViewModel(IUserService userService,IQrCodeGenerator generator,IMvxLogProvider logger,IMvxNavigationService nav):base(logger,nav)
        {
            _qrGenerator = generator;
            _userService = userService;
        }

        private object _qrCode;
        public object QrCode
        {
            get { return _qrCode; }
            set { _qrCode = value; RaisePropertyChanged(() => QrCode); }
        }

        private MvxCommand _closeCommand;
        public MvxCommand CloseCommand
        {
            get
            {
                _closeCommand = _closeCommand ?? new MvxCommand(() => NavigationService.Close(this));
                return _closeCommand;
            }
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            _userPhone = (await _userService.CurrentUser().ConfigureAwait(false)).Phone;
            QrCode = _qrGenerator.GenerateQrCode(_userPhone,800);
        }
    }
}

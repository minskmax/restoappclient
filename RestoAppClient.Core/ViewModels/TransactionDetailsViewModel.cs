﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using RestoAppClient.Core.ViewModels.Items;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class TransactionDetailsViewModel : MvxNavigationViewModel<TransactionItem>
    {
        private IUserService _userService;
        private IBonusesService _bonusesService;

        private TransactionItem _transaction;
        private User _creator;
        private User _owner;

        public TransactionDetailsViewModel(IBonusesService bonusService, IUserService service, IMvxLogProvider logProvider, IMvxNavigationService navigationService) : base(logProvider, navigationService)
        {
            _userService = service;
            _bonusesService = bonusService;
        }

        private MvxCommand _close;
        public MvxCommand CloseCommand
        {
            get
            {
                _close = _close ?? new MvxCommand(() => NavigationService.Close(this));
                return _close;
            }
        }

        private string _userName;
        public string UserName
        {
            get => _userName;
            set { _userName = value; RaisePropertyChanged(() => UserName); }
        }

        private string _userPhone;
        public string UserPhone
        {
            get => _userPhone;
            set { _userPhone = value; RaisePropertyChanged(() => UserPhone); }
        }

        private DateTime _created;
        public DateTime Created
        {
            get => _created;
            set { _created = value; RaisePropertyChanged(() => Created);RaisePropertyChanged(() => CreatedFormatted); }
        }

        public string CreatedFormatted
        {
            get => _created.ToLocalTime().ToString();
        }

        private string _creatorPhone;
        public string CreatorPhone
        {
            get => _creatorPhone;
            set { _creatorPhone = value; RaisePropertyChanged(() => CreatorPhone); }
        }

        private int _count;
        public int Count
        {
            get => _count;
            set { _count = value; RaisePropertyChanged(() => Count); }
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            _creator = await _userService.UserById(_transaction.CreatorId);
            _owner = await _userService.UserById(_transaction.OwnerId);

            UserName = _owner.Phone;
            UserPhone = _owner.Phone;
            Created = _transaction.Date;
            CreatorPhone = _creator.Phone;
            Count = _transaction.Count;            
        }

        public override void ViewAppeared()
        {
            //if (!_authService.IsAuthorized) NavigationService.Close(this);
        }
        
        public override void Prepare(TransactionItem parameter)
        {
            _transaction = parameter;
        }
    }
}

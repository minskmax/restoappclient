﻿using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;
using System.Threading.Tasks;

namespace RestoAppClient.Core.ViewModels
{
    public class TransactionMinusViewModel : TransactionViewModel
    {
        public TransactionMinusViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBonusesService service) : base(logProvider, navigationService, service)
        {
        }

        IMvxCommand _subBonuses;
        public IMvxCommand SubBonusesCommand
        {
            get
            {
                _subBonuses = _subBonuses ?? new MvxAsyncCommand(() => SubBonuses());
                return _subBonuses;
            }
        }

        private string _subCount;

        public string SubCount
        {
            get { return _subCount; }
            set
            {
                _subCount = value;
                BonusesCount = int.Parse(_subCount);
            }
        }


        private async Task SubBonuses()
        {
            var newTranscation = new BonusTransaction
            {
                Cause = "Android Client",
                UserPhone = Phone,
                Count = -BonusesCount
            };

            await _service.AddTransactionAsync(newTranscation);

            await NavigationService.Close(this, true);
        }
    }
}

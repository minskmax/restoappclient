﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.Navigation;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models;

namespace RestoAppClient.Core.ViewModels
{
    public class TransactionPlusViewModel : TransactionViewModel
    {
        public TransactionPlusViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBonusesService service) : base(logProvider, navigationService, service)
        {
        }

        IMvxCommand _addBonuses;
        public IMvxCommand AddBonusesCommand
        {
            get
            {
                _addBonuses = _addBonuses ?? new MvxAsyncCommand(() => AddBonuses());
                return _addBonuses;
            }
        }

        private async Task AddBonuses()
        {
            var tmp = OrderSumm;

            var newTranscation = new BonusTransaction
            {
                Cause = "Android Client",
                UserPhone = Phone,
                Count = BonusesCount
            };

            await _service.AddTransactionAsync(newTranscation);

            await NavigationService.Close(this,true);
        }
    }
}

﻿using MvvmCross.Logging;

using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RestoAppClient.Core.Interfaces.Rest;
using RestoAppClient.Core.Models.Dto;
using System;

namespace RestoAppClient.Core.ViewModels
{
    public class TransactionViewModel : MvxNavigationViewModel<TransactionArgs,bool>
    {
        protected const int percentOfOrder = 5;//TODO: Move it to backend!
        protected IBonusesService _service;

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; RaisePropertyChanged(() => Phone); }
        }               

        private string _orderSumm;
        public string OrderSumm
        {
            get { return _orderSumm; }
            set {
                SetBonusesCount(value);
                _orderSumm = value;
                RaisePropertyChanged(() => OrderSumm);
            }
        }

        private int _bonusesCount;
        public int BonusesCount
        {
            get { return _bonusesCount; }
            set { _bonusesCount = value;RaisePropertyChanged(() => BonusesCount); }
        }

        public TransactionViewModel(IMvxLogProvider logProvider, IMvxNavigationService navigationService, IBonusesService service) : base(logProvider, navigationService)
        {
            _service = service;
        }

        public override void Prepare(TransactionArgs parameter)
        {
            _phone = parameter.Phone;            
        }

        protected void SetBonusesCount(string order)
        {
            float orderSumm = float.Parse(order);
            BonusesCount = (int) ((orderSumm / 100f) * percentOfOrder);
        }
    }
}

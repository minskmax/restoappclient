﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.Graphics.Drawable;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;

namespace RestoAppClient.Droid.Adapters
{
    public class ImagesItemsAdapter: MvxRecyclerAdapter
    {
        private Context _androidContext;

        public ImagesItemsAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext)
        {
        }        

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, this.BindingContext.LayoutInflaterHolder);
            var view = this.InflateViewForHolder(parent, viewType, itemBindingContext);            

            _androidContext = parent.Context;
            return new ImagesViewHolder(view, itemBindingContext, _androidContext)
            {
                
            };
        }
    }

    public class ImagesViewHolder : MvxRecyclerViewHolder
    {
        private Context _androidContext;
        private ImageView _imageView;
        private AnimatedVectorDrawableCompat _glass;
        private Random _rnd = new Random();

        public ImagesViewHolder(View itemView, IMvxAndroidBindingContext context, Context androidContext)
                : base(itemView, context)
        {
            _androidContext = androidContext;
            _imageView = itemView.FindViewById<ImageView>(Resource.Id.image_loading);
            var glass = AnimatedVectorDrawableCompat.Create(androidContext, Resource.Drawable.glass);
            var rnd = _rnd.Next(50, 255);
            glass.SetAlpha(rnd);
            glass.RegisterAnimationCallback(
                new LoopCallback());
            _imageView.SetImageDrawable(glass);
            glass.Start();
        }
        public override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();
            //_glass.Start();
            //var glass = AnimatedVectorDrawableCompat.Create(this, Resource.Drawable.glass);
            //var img = FindViewById<ImageView>(Resource.Id.splash_image);
            //img.SetImageDrawable(_glass);
            //_animFadeIn = AnimationUtils.LoadAnimation(this, Resource.Animation.fadein);
            //_fading = FindViewById(Resource.Id.fading);
        }
        private class LoopCallback:Animatable2CompatAnimationCallback
        {
            public override void OnAnimationEnd(Drawable drawable)
            {
                (drawable as AnimatedVectorDrawableCompat)?.Start();
                (drawable as AnimatedVectorDrawable)?.Start();                
            }
        }
    }
}
﻿using Android.Content;
using Android.Graphics;
using Android.Support.Graphics.Drawable;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using RestoAppClient.Core.ViewModels.Items;
using RestoAppClient.Droid.Decorations;
using System.Collections;

namespace RestoAppClient.Droid.Adapters
{
    public class IncrementalAdapter : MvxRecyclerAdapter
    {
        private MvxRecyclerView childRecyclerView;
        private Context _androidContext;

        public IncrementalAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext)
        {
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, this.BindingContext.LayoutInflaterHolder);
            var view = this.InflateViewForHolder(parent, viewType, itemBindingContext);
            childRecyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.imagelist);
            var manager = new WrapLinearManager(parent.Context);

            var snapHelper = new PagerSnapHelper();
            snapHelper.AttachToRecyclerView(childRecyclerView);

            var decoration = new LinePagerIndicatorDecoration(parent.Context, 60, 4, 20, Color.Red, Color.DarkRed);
            childRecyclerView.AddItemDecoration(decoration);

            _androidContext = parent.Context;
            return new RecyclerViewHolder(view, itemBindingContext, _androidContext)
            {
                // Click = ItemClick
            };
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);
            
            //Custom Layout Manager for the horizontal scroll view of TimeSlots
            LinearLayoutManager layoutManager = new WrapLinearManager(_androidContext)
            {
                Orientation = LinearLayoutManager.Horizontal,
                ReverseLayout = false
            };

            

            childRecyclerView?.SetLayoutManager(layoutManager);
            childRecyclerView.Adapter = new ImagesItemsAdapter(BindingContext);





            //adjust width of time slot recycler view dynamically based on number of buttons on screen

            //DisplayMetrics displayMetrics = _androidContext.Resources.DisplayMetrics;

            //float screenWidth = displayMetrics.WidthPixels / displayMetrics.Density;

            //int buttonSize = 76 + 7;//76 is button width in dp and 7 is the margin
            //int numberOfButtons = (int)screenWidth / buttonSize;

            //float widthInDP = numberOfButtons * buttonSize + 7; //include the right margin in the total width
            //int widthInPixel = (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, widthInDP, displayMetrics);

            //ViewGroup.LayoutParams layoutSettings = childRecyclerView.LayoutParameters;
            //layoutSettings.Width = widthInPixel;

        }

        protected override void SetItemsSource(IEnumerable value)
        {
            base.SetItemsSource(value);
        }
        protected override int GetViewPosition(object item)
        {
            return base.GetViewPosition(item);
        }

        protected override int GetViewPosition(int itemsSourcePosition)
        {
            var test = itemsSourcePosition;
            return base.GetViewPosition(itemsSourcePosition);
        }

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }

        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public class RecyclerViewHolder : MvxRecyclerViewHolder
        {
            private Context _androidContext;
            public RecyclerViewHolder(View itemView, IMvxAndroidBindingContext context,Context androidContext)
                    : base(itemView, context)
            {
                _androidContext = androidContext;

            }
        }
    }
}
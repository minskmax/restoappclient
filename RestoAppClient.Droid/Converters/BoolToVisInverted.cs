﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Plugin.Visibility;
using MvvmCross.UI;

namespace RestoAppClient.Droid.Converters
{
    public class BoolToVisInverted : MvxBaseVisibilityValueConverter<bool>
    {
        protected override MvxVisibility Convert(bool value, object parameter, CultureInfo culture)
        {
            if (!value)
            {
                return MvxVisibility.Visible;
            }
            return MvxVisibility.Collapsed;
        }
    }
}
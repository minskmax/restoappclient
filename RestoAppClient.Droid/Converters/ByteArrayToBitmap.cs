﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Converters;
using RestoAppClient.Droid.Helpers;

namespace RestoAppClient.Droid.Converters
{
    public class ByteArrayToBitmap : MvxValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var data = value as byte[];
            //var task = Task.Run(async () =>
            //{
            if (data!=null)
                return BitmapFactory.DecodeByteArray(data, 0, data.Length);
            //    return bitmap;
            //});
            return null;
        }
        
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Converters;
using MvvmCross.Plugin.Visibility;
using MvvmCross.UI;

namespace RestoAppClient.Droid.Converters
{
    public class CollectionCountToVisible:MvxBaseVisibilityValueConverter<ICollection>
    {
        protected override MvxVisibility Convert(ICollection value, object parameter, CultureInfo culture)
        {
            if (value != null && value.Count > 0)
            {
                return MvxVisibility.Visible;
            }
            return MvxVisibility.Collapsed;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Plugin.Visibility;
using MvvmCross.UI;
using RestoAppClient.Core.Interfaces.Native;

namespace RestoAppClient.Droid.Converters
{
    public class DefaultBitmapToVis:MvxBaseVisibilityValueConverter<Bitmap>
    {
        IBitmapConverter _converter;
        public DefaultBitmapToVis():base()
        {
            //_converter = Mvx.IoCProvider.Resolve<IBitmapConverter>();
        }

        protected override MvxVisibility Convert(Bitmap value, object parameter, CultureInfo culture)
        {
            if (value == _converter.NativeBlankImage)
            {
                return MvxVisibility.Visible;
            }
            return MvxVisibility.Collapsed;
        }
    }
}
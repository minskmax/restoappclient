﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Converters;
using MvvmCross.Plugin.Color;
using MvvmCross.Plugin.Visibility;
using MvvmCross.UI;
using RestoAppClient.Core.Interfaces.Native;

namespace RestoAppClient.Droid.Converters
{
    public class IntToColor : MvxColorValueConverter<int?>
    {
        protected override MvxColor Convert(int? value, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var color = new MvxColor((int)value,255);
                return color;
            }
            return new MvxColor(Resource.Color.colorPrimaryDark);
        }
    }
}
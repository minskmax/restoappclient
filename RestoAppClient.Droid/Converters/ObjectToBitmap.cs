﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Converters;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Droid.Helpers;

namespace RestoAppClient.Droid.Converters
{
    public class ObjectToBitmap : MvxValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var data = (Bitmap)value;
            return data;
        }

    }
}
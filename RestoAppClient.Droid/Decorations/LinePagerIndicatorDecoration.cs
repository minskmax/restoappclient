﻿using Android.Content;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Views.Animations;
using System;

namespace RestoAppClient.Droid.Decorations
{
    class LinePagerIndicatorDecoration : RecyclerView.ItemDecoration
    {
        private float _indicatorItemLength;
        private float _itemPadding;
        private float _itemHeight;

        private Paint _paint;
        private Color _colorInactive;
        private Color _color;
        private AccelerateDecelerateInterpolator _interpolator;

        public LinePagerIndicatorDecoration(Context context, float itemLength, float itemHeight, float itemPadding, Color color, Color colorInactive)
        {
            _indicatorItemLength = itemLength;
            _itemPadding = itemPadding;
            _itemHeight = itemHeight;
            _colorInactive = colorInactive;
            _color = color;

            float thickness = TypedValue.ApplyDimension(ComplexUnitType.Dip,
                    itemHeight, context.Resources.DisplayMetrics);

            _paint = new Paint
            {
                Color = color,
                StrokeWidth = thickness
            };

            _interpolator = new AccelerateDecelerateInterpolator();
        }

        public override void OnDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state)
        {
            base.OnDrawOver(c, parent, state);

            var itemCount = parent.GetAdapter().ItemCount;

            // center horizontally, calculate width and subtract half from center
            var totalLength = _indicatorItemLength * itemCount;
            float paddingBetweenItems = Math.Max(0, itemCount - 1) * _itemPadding;
            float indicatorTotalWidth = totalLength + paddingBetweenItems;
            float indicatorStartX = (parent.Width - indicatorTotalWidth) / 2f;

            // center vertically in the allotted space
            float indicatorPosY = parent.Height - _itemHeight / 2f - 16;

            if (itemCount > 1)
            {
                DrawInactiveIndicators(c, indicatorStartX, indicatorPosY, itemCount);

                // find active page (which should be highlighted)
                var layoutManager = (LinearLayoutManager)parent.GetLayoutManager();
                int activePosition = layoutManager.FindFirstVisibleItemPosition();
                if (activePosition == RecyclerView.NoPosition)
                {
                    return;
                }

                // find offset of active page (if the user is scrolling)
                View activeChild = layoutManager.FindViewByPosition(activePosition);
                int left = activeChild.Left;
                int width = activeChild.Width;

                // on swipe the active item will be positioned from [-width, 0]
                // interpolate offset for smooth animation
                float progress = _interpolator.GetInterpolation(left * -1 / (float)width);

                DrawHighlights(c, indicatorStartX, indicatorPosY, activePosition, progress, itemCount);
            }
        }

        private void DrawInactiveIndicators(Canvas c, float indicatorStartX, float indicatorPosY, int itemCount)
        {
            _paint.Color = _colorInactive;

            // width of item indicator including padding
            float itemWidth = _indicatorItemLength + _itemPadding;

            float start = indicatorStartX;
            for (int i = 0; i < itemCount; i++)
            {
                // draw the line for every item
                c.DrawLine(start, indicatorPosY,
                   start + _indicatorItemLength, indicatorPosY, _paint);
                start += itemWidth;
            }
        }

        private void DrawHighlights(Canvas c, float indicatorStartX, float indicatorPosY,
                            int highlightPosition, float progress, int itemCount)
        {
            _paint.Color = _color;

            // width of item indicator including padding
            float itemWidth =_indicatorItemLength + _itemPadding;

            if (progress == 0f)
            {
                // no swipe, draw a normal indicator
                float highlightStart = indicatorStartX + itemWidth * highlightPosition;
                c.DrawLine(highlightStart, indicatorPosY,
                    highlightStart + _indicatorItemLength, indicatorPosY, _paint);
            }
            else
            {
                float highlightStart = indicatorStartX + itemWidth * highlightPosition;
                // calculate partial highlight
                float partialLength = _indicatorItemLength * progress;

                // draw the cut off highlight
                c.DrawLine(highlightStart + partialLength, indicatorPosY,
                    highlightStart + _indicatorItemLength, indicatorPosY, _paint);

                // draw the highlight overlapping to the next item as well
                if (highlightPosition < itemCount - 1)
                {
                    highlightStart += itemWidth;
                    c.DrawLine(highlightStart, indicatorPosY,
                        highlightStart + partialLength, indicatorPosY, _paint);
                }
            }
        }
    }
}
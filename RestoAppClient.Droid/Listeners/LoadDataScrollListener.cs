﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using static Android.Support.V7.Widget.RecyclerView;

namespace RestoAppClient.Droid.Listeners
{
    public class LoadDataScrollListener:OnScrollListener
    {
        private LinearLayoutManager _manager;
        private int _margin;

        public event EventHandler NeedMoreData;

        public LoadDataScrollListener(LinearLayoutManager manager,int scrollMargin):base()
        {
            _manager = manager;
            _margin = scrollMargin;
        }
        public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
        {
            base.OnScrolled(recyclerView, dx, dy);

            var visibleItemCount = recyclerView.ChildCount;
            var totalItemCount = _manager.ItemCount;
            var firstVisibleItem = _manager.FindFirstVisibleItemPosition();

            if (firstVisibleItem+visibleItemCount+_margin>=totalItemCount)
            {
                NeedMoreData?.Invoke(this,new ScrollEventArgs { LastItem = totalItemCount });
            }
        }
        public void Finish()
        {
            NeedMoreData = null;
        }
    }

    public class ScrollEventArgs:EventArgs
    {
        public int LastItem { get; set; }
    }
}
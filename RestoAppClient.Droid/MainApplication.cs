﻿using System;
using Android.App;
using Android.Runtime;
using Android.Support.V7.App;
using MvvmCross.Droid.Support.V7.AppCompat;
using Plugin.CurrentActivity;
using RestoAppClient.Core;

namespace RestoAppClient.Droid
{
    [Application]
    public class MainApplication : MvxAppCompatApplication<Setup, App>
    {
        public MainApplication(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
            
        }

        public override void OnCreate()
        {
            base.OnCreate();
            AppCompatDelegate.CompatVectorFromResourcesEnabled = true;
            CrossCurrentActivity.Current.Init(this);
        }
    }
}
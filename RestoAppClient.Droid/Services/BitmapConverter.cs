﻿using Android.Content.Res;
using Android.Graphics;
using MvvmCross;
using MvvmCross.Platforms.Android;
using RestoAppClient.Core.Interfaces.Native;
using System.Threading.Tasks;

namespace RestoAppClient.Droid.Services
{
    public class BitmapConverter : IBitmapConverter
    {
        private Bitmap _emptyBitmap;
        public object NativeBlankImage
        {
            get
            {
                if (_emptyBitmap != null) return _emptyBitmap;

                _emptyBitmap = BitmapFactory.DecodeResource(GetResources(), Resource.Drawable.loading_image);

                //_emptyBitmap = Bitmap.CreateBitmap(64, 64, Bitmap.Config.RgbaF16);
                //_emptyBitmap.EraseColor(Color.White);
                return _emptyBitmap;
            }
        }

        private Resources GetResources()
        {
            var topActivity = Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>();
            return topActivity.Activity.Resources;
        }

        public async Task<object> FromByteArray(byte[] data)
        {
            if (data == null || data.Length < 1) return _emptyBitmap;

            BitmapFactory.Options options = new BitmapFactory.Options
            {
                InPurgeable = true // inPurgeable is used to free up memory while required
            };
            //var bitmapDispose = nativeBitmapToDispose as Bitmap;
            //if (bitmapDispose!=null && !bitmapDispose.IsRecycled)
            //{
            //    bitmapDispose.Recycle();
            //}
            //System.GC.Collect();
            Bitmap result;
            try
            {
                result = await BitmapFactory.DecodeByteArrayAsync(data, 0, data.Length);
            }
            catch (System.Exception)
            {
                result = _emptyBitmap;
                System.GC.Collect();
            }
            return result;
        }

        private static int CalculateInSampleSize(
        BitmapFactory.Options options, int reqWidth, int reqHeight)
        {
            // Raw height and width of image
            var height = options.OutHeight;
            var width = options.OutWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth)
            {

                int halfHeight = height / 2;
                int halfWidth = width / 2;

                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width larger than the requested height and width.
                while ((halfHeight / inSampleSize) > reqHeight
                        && (halfWidth / inSampleSize) > reqWidth)
                {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

    }
}
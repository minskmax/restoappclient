﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace RestoAppClient.Droid.Services.DbModels
{
    public class NewsDbModel
    {
        [PrimaryKey, Column("_id")]
        public int Id { get; set; }        
        public string Data { get; set; }
        public long Modified { get; set; }
        public long Created { get; set; }
    }
}
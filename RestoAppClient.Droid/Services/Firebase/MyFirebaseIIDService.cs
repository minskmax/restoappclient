﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Firebase.Iid;
using MvvmCross;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Rest;

namespace RestoAppClient.Droid.Services.Firebase
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";
        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed token: " + refreshedToken);
            SendRegistrationToServer(refreshedToken);
        }
        void SendRegistrationToServer(string token)
        {
            // Add custom implementation, as needed.            
            Log.Debug(TAG, "send update token:" + token);
            var service = Mvx.IoCProvider.Resolve<IFirebaseService>();
            var tokenStore = Mvx.IoCProvider.Resolve<ITokenStore>();
            
            var apiToken = tokenStore.LoadToken();
            if (apiToken != null)
            {
                var restService = Mvx.IoCProvider.Resolve<IRestClient>();
                restService.UpdateToken(apiToken);
                var result = service.UpdateToken(token).Result;
                Log.Debug(TAG, "update token result:" + result);
            }
        }
    }

}
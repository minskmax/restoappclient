﻿using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Android.Util;
using Firebase.Messaging;
using MvvmCross;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Droid.Views;
using RestoAppClient.Droid.Views.Fragments;
using System.Collections.Generic;

namespace RestoAppClient.Droid.Services.Firebase
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";
        //private IToaster _toaster;

        public MyFirebaseMessagingService() : base()
        {
            //_toaster = Mvx.IoCProvider.Resolve<IToaster>();
        }

        public override void OnMessageReceived(RemoteMessage message)
        {
            Log.Debug(TAG, "FCM message received!");
            var notification = message.GetNotification();

            //Log.Debug(TAG, "Notification Message Body: " + notification.Body);
            SendNotification(notification.Body,notification.Title, message.Data);

            var intent = new Intent(this, typeof(MainContainerActivity));
            intent.PutExtra("Fragment", "Bonuses".ToString());
            var tmp = intent.GetStringExtra("Fragment");

            this.StartActivity(intent);
            //_toaster.ShowMessage(notification.Title, notification.Body, null);            
        }

        void SendNotification(string messageBody, string messageHeader, IDictionary<string, string> data)
        {
            var intent = new Intent(this, typeof(MainContainerActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            foreach (var key in data.Keys)
            {
                intent.PutExtra(key, data[key]);
            }

            var pendingIntent = PendingIntent.GetActivity(this, MainContainerActivity.NOTIFICATION_ID, intent, PendingIntentFlags.OneShot);

            var notificationBuilder = new NotificationCompat.Builder(this, MainContainerActivity.CHANNEL_ID)
                                      .SetSmallIcon(Resource.Drawable.ic_arrow_back)
                                      .SetContentTitle(messageHeader)
                                      .SetContentText(messageBody)
                                      .SetAutoCancel(true)
                                      .SetContentIntent(pendingIntent);

            var notificationManager = NotificationManagerCompat.From(this);
            notificationManager.Notify(MainContainerActivity.NOTIFICATION_ID, notificationBuilder.Build());
        }
    }
}
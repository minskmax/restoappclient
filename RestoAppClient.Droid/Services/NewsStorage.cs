﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RestoAppClient.Core.Helpers;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Droid.Services.DbModels;
using SQLite;

namespace RestoAppClient.Droid.Services
{
    public class NewsStorage : IDataCache
    {
        private readonly string _dbPath;
        private readonly string _dbName = "restoapp";
        private readonly int _maxCount = 50;
        private SQLiteAsyncConnection _db;
        private bool _tableCreated;
        private static readonly AsyncLock Mutex = new AsyncLock();


        public NewsStorage()
        {
            _dbPath = Path.Combine(
        Environment.GetFolderPath(Environment.SpecialFolder.Personal),
        $"{_dbName}.db3");
            // dbPath contains a valid file path for the database file to be stored
            _db = new SQLiteAsyncConnection(_dbPath);
            //_db.DropTableAsync<NewsDbModel>().Wait();
            //_db.DeleteAllAsync<NewsDbModel>().Wait();
            //_db.CloseAsync();
        }

        public async Task CreateTable()
        {
            if (!_tableCreated)
            {
                using (await Mutex.LockAsync().ConfigureAwait(false))
                {
                    await _db.CreateTableAsync<NewsDbModel>().ConfigureAwait(false);
                    _tableCreated = true;
                }
            }
        }

        public async Task<bool> DeleteData(int key)
        {
            await CreateTable().ConfigureAwait(false);

            var result = await _db.DeleteAsync<NewsDbModel>(key);

            if (result > 0) return true;
            return false;
        }

        public async Task<string> TryLoadData(int key)
        {
            await CreateTable().ConfigureAwait(false);
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {
                return (await _db.GetAsync<NewsDbModel>(key).ConfigureAwait(false)).Data;
            }
        }

        public async Task<bool> UpdateData(int key, string value, DateTime modified, DateTime created)
        {
            await CreateTable().ConfigureAwait(false);
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {


                NewsDbModel adding =
                new NewsDbModel { Id = key, Data = value, Modified = modified.Ticks, Created = created.Ticks };
                NewsDbModel exist = null;
                try
                {
                    exist = await _db.GetAsync<NewsDbModel>(key).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    var tmp = e.Message;
                    var inner = e.InnerException;
                }

                if (exist == null)
                {
                    var result = await _db.InsertAsync(adding).ConfigureAwait(false);
                    return true;
                }
                else if (exist.Modified < modified.Ticks)
                {

                    exist.Data = value;
                    exist.Modified = DateTime.UtcNow.Ticks;
                    var result = await _db.UpdateAsync(exist).ConfigureAwait(false);
                    if (result > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public async Task<bool> TryGetSpace(int id, DateTime created)
        {
            await CreateTable().ConfigureAwait(false);
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {

                NewsDbModel exist;
                try
                {
                    exist = await _db.GetAsync<NewsDbModel>(id).ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    exist = null;
                }

                if (exist != null) return true;
                var count = await _db.Table<NewsDbModel>().CountAsync().ConfigureAwait(false);
                if (count < _maxCount) return true;

                var oldestItem = await _db.Table<NewsDbModel>().Where(x => x.Created < created.Ticks).OrderBy(x => x.Created).FirstOrDefaultAsync().ConfigureAwait(false);
                if (oldestItem != null)
                {
                    await _db.DeleteAsync<NewsDbModel>(oldestItem.Id).ConfigureAwait(false);
                    return true;
                }


                return false;
            }
        }

        public async Task<bool> IsNeedUpdateAsync(int id, DateTime modified)
        {
            await CreateTable().ConfigureAwait(false);
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {
                NewsDbModel exist;
                try
                {
                    exist = await _db.GetAsync<NewsDbModel>(id).ConfigureAwait(false);
                }
                catch (Exception e)
                {

                    exist = null;
                }
                if (exist == null)
                {
                    return true;
                }
                else if (exist.Modified < modified.Ticks)
                {
                    return true;
                }
                return false;
            }
        }

        public async Task<IEnumerable<KeyValuePair<int,string>>> LoadCache()
        {
            await CreateTable().ConfigureAwait(false);
            using (await Mutex.LockAsync().ConfigureAwait(false))
            {

                var items = await _db.Table<NewsDbModel>().OrderByDescending(x => x.Created).ToListAsync().ConfigureAwait(false);
                var result = new List<KeyValuePair<int, string>>();
                foreach (var item in items)
                {
                    result.Add(new KeyValuePair<int, string>(item.Id, item.Data));
                }
                return result;
            }
        }        
    }
}
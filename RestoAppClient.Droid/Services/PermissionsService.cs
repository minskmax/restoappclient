﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.CurrentActivity;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Droid.Services.Interfaces;

namespace RestoAppClient.Droid.Services
{
    public class PermissionsService : IPermissionsService
    {
        public async Task<bool> RequestCamera(Context context, string requestMessage)
        {
            try
            {
                var activity = CrossCurrentActivity.Current;
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                if (status != PermissionStatus.Granted)
                {
                    var needAsk = await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera);
                    if (needAsk)
                    {
                        //Toast.MakeText(context, requestMessage, ToastLength.Long).Show();
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Camera))
                        status = results[Permission.Camera];
                }

                if (status == PermissionStatus.Granted)
                {
                    return true;
                }
                else if (status != PermissionStatus.Unknown)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public async Task<bool> RequestSmsReceive(Context context, string requestMessage)
        {
            try
            {
                var activity = CrossCurrentActivity.Current;
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Sms);
                if (status != PermissionStatus.Granted)
                {
                    var needAsk = await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Sms);
                    if (needAsk)
                    {
                        //Toast.MakeText(context, requestMessage, ToastLength.Long).Show();
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Sms);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Sms))
                        status = results[Permission.Sms];
                }

                if (status == PermissionStatus.Granted)
                {
                    return true;                    
                }
                else if (status != PermissionStatus.Unknown)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
            return false;
        }
    }
}
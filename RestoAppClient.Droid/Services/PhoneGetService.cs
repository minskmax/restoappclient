﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Telephony;
using Android.Views;
using Android.Widget;
using Plugin.CurrentActivity;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using RestoAppClient.Core.Interfaces;

namespace RestoAppClient.Droid.Services
{
    public class PhoneGetService : IPhoneGetService
    {
        public async Task<string> GetPhoneNumber()
        {
            try
            {
                var activity = CrossCurrentActivity.Current;
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Contacts);
                if (status != PermissionStatus.Granted)
                {
                    var needAsk = await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location);
                    if (needAsk)
                    {
                        //await DisplayAlert("Need contacts", "Gunna need that location", "OK");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Contacts);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Contacts))
                        status = results[Permission.Contacts];
                }

                if (status == PermissionStatus.Granted)
                {
                    TelephonyManager mTelephonyMgr;

                    mTelephonyMgr = (TelephonyManager)Application.Context.GetSystemService("phone");

                    var Number = mTelephonyMgr.Line1Number;

                    return Number;
                }
                else if (status != PermissionStatus.Unknown)
                {
                    return "failed";
                }
            }
            catch (Exception ex)
            {

                return "exception "+ex.Message;
            }
            
            return "Phone number";
        }
        
    }
}
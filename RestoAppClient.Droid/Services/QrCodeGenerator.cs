﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RestoAppClient.Core.Interfaces.Native;
using SkiaSharp;
using ZXing.Common;
using ZXing.Mobile;
using ZXing.QrCode;

namespace RestoAppClient.Droid.Services
{
    public class QrCodeGenerator : IQrCodeGenerator
    {
        public object GenerateQrCode(string url, int width=300)
        {
            var writer = new QRCodeWriter();
            BitMatrix bm = writer.encode(url,ZXing.BarcodeFormat.QR_CODE,width,width);
            BitmapRenderer bitmap = new BitmapRenderer();
            var image = bitmap.Render(bm, ZXing.BarcodeFormat.QR_CODE, url);
            return image;
        }

        private SKPath BitMatrixToPath(BitMatrix matrix)
        {
            var path = new SKPath();
            for (int x = 0; x < matrix.Width; x++)
            {
                for (int y = 0; y < matrix.Height; y++)
                {
                    path.AddRect(
                        new SKRect(x, y, x + 1, y + 1)
                        );
                }
            }
            return path;
        }
    }
}
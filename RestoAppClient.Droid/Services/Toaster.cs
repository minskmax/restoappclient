﻿using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using Plugin.CurrentActivity;
using RestoAppClient.Core.Interfaces.Native;
using System;

namespace RestoAppClient.Droid.Services
{
    public class Toaster : IToaster
    {

        public void ShowMessage(string header, string body, Action onClick)
        {
            var activity = Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity as IMvxBindingContextOwner;
            // note that we're not using Binding in this Inflation - but the overhead is minimal - so use it anyway!
            View layoutView = activity.BindingInflate(Resource.Layout.toast, null);

            var text1 = layoutView.FindViewById<TextView>(Resource.Id.ErrorText1);
            text1.Text = header;
            var text2 = layoutView.FindViewById<TextView>(Resource.Id.ErrorText2);
            text2.Text = body;

            var currentActivity = CrossCurrentActivity.Current.Activity;
            currentActivity.RunOnUiThread(() =>
            {
                var toast = new Toast(CrossCurrentActivity.Current.Activity);
                toast.SetGravity(GravityFlags.CenterVertical, 0, 0);
                toast.Duration = ToastLength.Long;
                toast.View = layoutView;
                toast.Show();
            });

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Iid;
using RestoAppClient.Core.Interfaces.Native;

namespace RestoAppClient.Droid.Services
{
    public class TokenAccess : ITokenAccess
    {
        public string GetFirebaseToken()
        {
            return FirebaseInstanceId.Instance.Token;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RestoAppClient.Core.Interfaces;
using Xamarin.Auth;

namespace RestoAppClient.Droid.Services
{
    public class TokenStore : ITokenStore
    {
        private Account _account;

        public async Task<string> LoadTokenAsync()
        {
            var accounts = await AccountStore.Create(Application.Context, "sinebruhof").FindAccountsForServiceAsync("restoapp");

            if (accounts!=null && accounts.Count>0)
            {
                _account = accounts[0];
                var token = _account.Properties["secret"];
                return token;
            }

            return null;
        }

        public string LoadToken()
        {
            var accounts = AccountStore.Create(Application.Context, "sinebruhof").FindAccountsForService("restoapp").ToList();

            if (accounts != null && accounts.Count > 0)
            {
                _account = accounts[0];
                var token = _account.Properties["secret"];
                return token;
            }

            return null;
        }

        public async Task StoreToken(string token)
        {
            if (_account == null)
            {
                _account = new Account("restoapp",
                    new Dictionary<string, string> { { "secret", token } });
            }
            else
            {
                _account.Properties["secret"] = token;
            }

            await AccountStore.Create(Application.Context,"sinebruhof").SaveAsync(_account,"restoapp");
        }
    }
}
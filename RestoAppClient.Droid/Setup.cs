﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.IoC;
using MvvmCross.Platforms.Android.Core;
using RestoAppClient.Core;
using RestoAppClient.Core.Interfaces;
using RestoAppClient.Core.Interfaces.Native;
using RestoAppClient.Droid.Services;
using RestoAppClient.Droid.Services.Interfaces;

namespace RestoAppClient.Droid
{
    public class Setup: MvxAppCompatSetup<App>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IPhoneGetService, PhoneGetService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IPermissionsService, PermissionsService>();
            //Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IBitmapConverter, BitmapConverter>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IQrCodeGenerator, QrCodeGenerator>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<ITokenStore, TokenStore>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IDataCache, NewsStorage>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<IToaster, Toaster>();
            Mvx.IoCProvider.ConstructAndRegisterSingleton<ITokenAccess, TokenAccess>();
        }                    
    }
}
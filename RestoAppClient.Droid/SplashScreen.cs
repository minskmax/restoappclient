﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.Graphics.Drawable;
using Android.Util;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Views;
using static Android.Resource;

namespace RestoAppClient.Droid
{
    [Activity(
         Label = "Синебрюхов"
         , MainLauncher = true
         , Icon = "@mipmap/ic_launcher"
         , Theme = "@style/AppTheme.Splash"
         , NoHistory = true
         , ScreenOrientation = ScreenOrientation.Portrait)]

    public class SplashScreen : MvxSplashScreenAppCompatActivity
    {
        private Android.Views.Animations.Animation _animFadeIn;
        private View _fading;
        private bool _attached;
        private AnimatedVectorDrawableCompat _glass;

        public SplashScreen() : base(Resource.Layout.splash_screen)
        {

        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            _glass = AnimatedVectorDrawableCompat.Create(this, Resource.Drawable.glass);            
            var img = FindViewById<ImageView>(Resource.Id.splash_image);
            img.SetImageDrawable(_glass);
            _animFadeIn = AnimationUtils.LoadAnimation(this, Resource.Animation.fadein);
            _fading = FindViewById(Resource.Id.fading);
        }

        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
            //SetContentView(Resource.Layout.splash_screen);
            var drawabl = AnimatedVectorDrawableCompat.Create(this, Resource.Drawable.glass);

            _animFadeIn = AnimationUtils.LoadAnimation(this, Resource.Animation.fadein);
            _fading = FindViewById(Resource.Id.fading);         
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            if (hasFocus && _attached)
            {
                if (_fading != null && _animFadeIn!=null)
                {
                    _fading.StartAnimation(_animFadeIn);                    
                }
                if (_glass!=null)
                {
                    _glass.Start();
                }
            }
        }

        public override void OnAttachedToWindow()
        {
            _attached = true;
        }
    }
}
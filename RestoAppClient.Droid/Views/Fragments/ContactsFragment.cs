﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Plugin.Permissions;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Droid.Base;

namespace RestoAppClient.Droid.Views.Fragments
{
    [MvxFragmentPresentation(typeof(MainContainerViewModel), Resource.Id.content_frame, AddToBackStack = false,
        EnterAnimation = Resource.Animation.design_bottom_sheet_slide_in,
        ExitAnimation = Resource.Animation.design_bottom_sheet_slide_out),]
    public class ContactsFragment : BaseFragment<ContactsViewModel>
    {
        protected override int FragmentLayoutId => Resource.Layout.contacts;

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
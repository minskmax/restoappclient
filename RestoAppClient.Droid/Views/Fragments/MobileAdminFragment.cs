﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Plugin.Permissions;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Droid.Base;
using RestoAppClient.Droid.Services;
using ZXing.Mobile;

namespace RestoAppClient.Droid.Views.Fragments
{
    [MvxFragmentPresentation(typeof(MainContainerViewModel), Resource.Id.content_frame, AddToBackStack = false,
    EnterAnimation = Resource.Animation.design_bottom_sheet_slide_in,
    ExitAnimation = Resource.Animation.design_bottom_sheet_slide_out),]
    public class MobileAdminFragment : BaseFragment<MobileAdminViewModel>
    {
        protected override int FragmentLayoutId => Resource.Layout.mobile_admin;        

        public override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            //System.Threading.Tasks.Task.Run(async () =>
            //{
            //    var permissions = new PermissionsService();
            //    await permissions.RequestCamera(Activity.ApplicationContext, "Подтвердите права на использование камеры, для сканирования QR кодов");
            //});

            Button scanBtn = view.FindViewById<Button>(Resource.Id.btn_scn);            

            scanBtn.Click += async (sender, e) => {

                // you don not create a new instance of the Android Application 
                // but get the one already created. From an activity you 
                // just call `Application` but from inside a fragment you need to get 
                // the fragment's activity then get the Application.

                MobileBarcodeScanner.Initialize(Activity.Application);                

                var scanner = new MobileBarcodeScanner();


                var opts = new MobileBarcodeScanningOptions
                {
                    PossibleFormats = new List<ZXing.BarcodeFormat> {
                    ZXing.BarcodeFormat.QR_CODE},
                    TryHarder = true,
                    AutoRotate = false
                };

                var result = await scanner.Scan(opts);

                // The if was inverted.
                if (result == null)
                {
                    return;
                }

                //Console.WriteLine($"Scanned Barcode: {result}");

                // Using this you are sure it will run in the UI thread
                // as you will be updating an UI element.
                Activity.RunOnUiThread(async () => {                    
                    await ViewModel.UpdatePhone(result.Text);
                });

            };


            return view;
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
﻿
using Android.App;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Support.Design.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using Plugin.Permissions;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Droid.Adapters;
using RestoAppClient.Droid.Listeners;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using RestoAppClient.Droid.Base;
using Android.Views;
using System;

namespace RestoAppClient.Droid.Views
{
    [MvxFragmentPresentation(typeof(MainContainerViewModel),
        Resource.Id.content_frame,Tag ="news",
        AddToBackStack = false,
        EnterAnimation =Resource.Animation.design_bottom_sheet_slide_in,
        ExitAnimation =Resource.Animation.design_bottom_sheet_slide_out)]
    public class NewsListView : BaseFragment<NewsListViewModel>
    {
        protected override int FragmentLayoutId => Resource.Layout.news;

        private MvxRecyclerView _list;
        private LinearLayoutManager _manager;
        private LoadDataScrollListener _listener;

        private int _lastScrollItem;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            _list = (MvxRecyclerView)view.FindViewById<MvxRecyclerView>(Resource.Id.news);
            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            if (_list != null)
            {
                _manager = new WrapLinearManager(Activity);
                _list.SetLayoutManager(_manager);
                _list.Adapter = new IncrementalAdapter((IMvxAndroidBindingContext)BindingContext);
                var collection = ViewModel.News;
                //subscribe to scroll to top
                ViewModel.NeedScrollToTop += NeedScrollToTop;
                _listener = new LoadDataScrollListener(_manager, 5);
                _list.AddOnScrollListener(_listener);

                Drawable horizontalDivider = ContextCompat.GetDrawable(Activity, Resource.Drawable.horizontal_divider);

                DividerItemDecoration horizontalDecoration = new DividerItemDecoration(_list.Context,
                        DividerItemDecoration.Vertical);

                horizontalDecoration.SetDrawable(horizontalDivider);
                _list.AddItemDecoration(horizontalDecoration);                

                _listener.NeedMoreData += async (s, e) =>
                {
                    var scrollArgs = e as ScrollEventArgs;
                    if (scrollArgs!=null && _lastScrollItem<scrollArgs.LastItem)
                    {
                        //_lastScrollItem = scrollArgs.LastItem; //TODO may be remove this? needs testing
                        await collection.LoadMoreDataAsync(false);
                    }
                };
            }
        }

        private void NeedScrollToTop(object sender, EventArgs args)
        {
                _manager.ScrollToPosition(0);
        }

        public override void OnDestroy()
        {
            _listener.Finish();
            base.OnDestroy();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
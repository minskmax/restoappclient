﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Google.Android.Gms.Auth.Api.Phone;
using Java.Lang;
using RestoAppClient.Core.ViewModels.Admin;
using RestoAppClient.Droid.Base;
using RestoAppClient.Droid.Services;

namespace RestoAppClient.Droid.Views
{
    [Activity(Label = "LoginActivity")]
    public class LoginActivity : BaseActivity<LoginAuthViewModel>
    {
        protected override int ActivityLayoutId => Resource.Layout.login;
        private EditText _code;
        private SMSBroadcastReceiver _receiver;        

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            var button = FindViewById<AppCompatButton>(Resource.Id.btn_login);
            _code = FindViewById<EditText>(Resource.Id.input_code);
            ViewModel.PropertyChanged += ChangeFocus;

            //sms broadcastlisten
            _receiver = new SMSBroadcastReceiver();
            RegisterReceiver(_receiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
           _receiver.SmsReceived += OnSms;

            //Retivier API
            //SmsRetrieverClient client = SmsRetriever.GetClient(this);
            //var retrivierTask = client.StartSmsRetriever();
            //retrivierTask.AddOnSuccessListener(this, new SuccessListener());
            //RegisterReceiver(new SmsAPIBroadcastReceiver(), new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

            //permissions to receive sms
            System.Threading.Tasks.Task.Run(async () =>
            {
                var permissions = new PermissionsService();
                await permissions.RequestSmsReceive(ApplicationContext, "Подтвердите права на получение смс для автоматической вставки кода");
            });            
        }

        private void OnSms(object sender, string sms)
        {
            if (!string.IsNullOrEmpty(sms))
            {
                _code.SetText(new System.String(sms.Where(Char.IsDigit).ToArray()), TextView.BufferType.Normal);
                //_receiver.SmsReceived -= OnSms;
            }
        }

        protected override void OnDestroy()
        {
            UnregisterReceiver(_receiver);
            _receiver.Finish();
            _receiver = null;
            base.OnDestroy();
        }

        private void ChangeFocus(object sender, EventArgs args)
        {
            var arg = (PropertyChangedEventArgs)args;
            if (arg.PropertyName=="CodeRequested")
            {
                _code.Visibility = ViewStates.Visible;
                _code.RequestFocus();
            }            
        }        
    }
}
﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Util;
using Android.Views;
using Firebase.Iid;
using Firebase.Messaging;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using MvvmCross.Binding.BindingContext;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Droid.Base;
using System;
using System.Windows.Input;

namespace RestoAppClient.Droid.Views
{
    [Activity(
         Theme = "@style/AppTheme",
         WindowSoftInputMode = SoftInput.AdjustResize | SoftInput.StateAlwaysHidden,
        LaunchMode = LaunchMode.SingleTask)]
    public class MainContainerActivity : BaseActivity<MainContainerViewModel>
    {
        internal static readonly string CHANNEL_ID = "sinebruhof";//For notifications
        internal static readonly int NOTIFICATION_ID = 100;


        protected override int ActivityLayoutId => Resource.Layout.root_view;

        private BottomNavigationView _bottomNavigation;
        private IMenuItem _adminMenu;
        private IMenuItem _bonusesMenu;

        public ICommand GoToNewsCommand { get; set; }
        public ICommand GoToBonusesCommand { get; set; }
        public ICommand GoToAdminCommand { get; set; }
        public ICommand GoToContactsCommand { get; set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            //AddBackgroundAnimations();

            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Log.Debug("Key: {0} Value: {1}", key, value);
                }
            }
            CreateNotificationChannel();
            IsPlayServicesAvailable();
            FirebaseMessaging.Instance.SubscribeToTopic("news");
            MetricsManager.Register(Application, "fd58ae89dd2a41d0a2927810aa3b655f");
            CheckForUpdates();
            AddBottomNavigation();
            Log.Debug("TOKEN:", "InstanceID token: " + FirebaseInstanceId.Instance.Token);
        }

        protected override void OnResume()
        {
            base.OnResume();
            ViewModel.AdminChanged += ShowHideAdminMenu;
            CrashManager.Register(this, "fd58ae89dd2a41d0a2927810aa3b655f");
            Log.Debug("TOKEN:", "InstanceID token: " + FirebaseInstanceId.Instance.Token);
        }

        protected override void OnNewIntent(Intent intent)
        {
            string fragment = intent.GetStringExtra("Fragment");
            if (fragment == "Bonuses")
            {
                var navargs = new BottomNavigationView.NavigationItemSelectedEventArgs(false, _bonusesMenu);
                BottomNavigation_NavigationItemSelected(this, navargs);
                _bottomNavigation.SelectedItemId = _bonusesMenu.ItemId;

                //// Create new fragment and transaction
                //var newFragment = new BonusesFragment();
                //var transaction = SupportFragmentManager.BeginTransaction();

                //// Replace whatever is in the fragment_container view with this fragment,
                //// and add the transaction to the back stack if needed
                //transaction.Replace(Resource.Id.content_frame, newFragment);
                //transaction.AddToBackStack(null);

                //// Commit the transaction
                //transaction.Commit();
            }
        }

        private void AddBottomNavigation()
        {
            _bottomNavigation = (BottomNavigationView)FindViewById(Resource.Id.bottom_navigation);
            ViewModel.AdminChanged += ShowHideAdminMenu;

            if (_bottomNavigation != null)
            {
                _bottomNavigation.NavigationItemSelected += BottomNavigation_NavigationItemSelected;
                // trying to bind command to view model property
                var set = this.CreateBindingSet<MainContainerActivity, MainContainerViewModel>();
                set.Bind(this).For(v => v.GoToNewsCommand).To(vm => vm.NavigateToNewsCommand);
                set.Bind(this).For(v => v.GoToBonusesCommand).To(vm => vm.NavigateToBonusesCommand);
                set.Bind(this).For(v => v.GoToAdminCommand).To(vm => vm.NavigateToAdminCommand);
                set.Bind(this).For(v => v.GoToContactsCommand).To(vm => vm.NavigateToContactsCommand);
                set.Apply();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Bottom navigation menu is null");
            }

            _bonusesMenu = _bottomNavigation.Menu.FindItem(Resource.Id.action_bonuses);
        }

        private void ShowHideAdminMenu(object sender, bool admin)
        {
            if (admin)
            {
                RunOnUiThread(() =>
                {
                    if (_adminMenu == null && _bottomNavigation.ChildCount<4)
                    {
                        _adminMenu = _bottomNavigation.Menu.Add("Админка");
                        _adminMenu.SetIcon(Resource.Drawable.common_google_signin_btn_icon_dark_normal);
                    }
                });
            }
            else
            {
                RunOnUiThread(() =>
                {
                    if (_adminMenu != null)
                    {
                        _bottomNavigation.Menu.RemoveItem(_adminMenu.ItemId);
                    }
                });
            }
        }

        private void BottomNavigation_NavigationItemSelected(object sender, BottomNavigationView.NavigationItemSelectedEventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine($"Bottom navigation menu is selected: {e.Item.ItemId}");

                if (e.Item.ItemId == Resource.Id.action_news)
                    if (GoToNewsCommand != null && GoToNewsCommand.CanExecute(null))
                        GoToNewsCommand.Execute(null);
                if (e.Item.ItemId == Resource.Id.action_bonuses)
                    if (GoToBonusesCommand != null && GoToBonusesCommand.CanExecute(null))
                        GoToBonusesCommand.Execute(null);
                if (e.Item.ItemId == Resource.Id.action_contacts)
                    if (GoToContactsCommand != null && GoToContactsCommand.CanExecute(null))
                        GoToContactsCommand.Execute(null);
                if (e.Item.ItemId == _adminMenu.ItemId)
                    if (GoToAdminCommand != null && GoToAdminCommand.CanExecute(null))
                        GoToAdminCommand.Execute(null);
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine($"Exception: {exception.Message}");
                //Crashes.TrackError(exception);
            }
        }

        private void CheckForUpdates()
        {
            // Remove this for store builds!
            UpdateManager.Register(this, "fd58ae89dd2a41d0a2927810aa3b655f");
        }
        private void UnregisterManagers()
        {
            UpdateManager.Unregister();
        }

        protected override void OnPause()
        {
            base.OnPause();
            ViewModel.AdminChanged -= ShowHideAdminMenu;
            UnregisterManagers();
        }        

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ViewModel.AdminChanged -= ShowHideAdminMenu;
            UnregisterManagers();
        }

        //Notifications
        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID,
                                                  "sinebruhof",
                                                  NotificationImportance.High)
            {

                Description = "Firebase Cloud Messages appear in this channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

        public bool IsPlayServicesAvailable()
        {
            var resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    Log.Debug("FMS", GoogleApiAvailability.Instance.GetErrorString(resultCode));
                }
                else
                {
                    Log.Debug("FMS", "This device is not supported");
                    Finish();
                }

                return false;
            }

            Log.Debug("FMS:", "Google Play Services is available.");
            return true;
        }
    }

}
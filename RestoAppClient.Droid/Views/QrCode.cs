﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Droid.Base;

namespace RestoAppClient.Droid.Views
{
    [Activity(Label = "QrCode")]
    public class QrCode : BaseActivity<QrCodeViewModel>
    {
        protected override int ActivityLayoutId => Resource.Layout.qrcode;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
        }
    }
}
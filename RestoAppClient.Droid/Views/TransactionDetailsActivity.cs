﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RestoAppClient.Core.ViewModels;
using RestoAppClient.Droid.Base;

namespace RestoAppClient.Droid.Views
{
    [Activity(Label = "TransactionDetails")]
    public class TransactionDetailsActivity : BaseActivity<TransactionDetailsViewModel>
    {
        protected override int ActivityLayoutId => Resource.Layout.transaction_details;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);            
        }
    }
}
Client applications for RestoApp system.

Xamarin, wpf, MVVMCross framework

  1. Windows WPF project - admin app: managing users, news, orders and bonuses.
  2. Core project - common models, interfaces and business logic. provided as Net. Standart library
  3. Android project - implementation of android UI with data bindings to models from Core. Implementation of platform specific services (e.g. Native Bitmap, Permissions, Protected Account Storage)